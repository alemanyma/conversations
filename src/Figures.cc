#include "../include/Figures.h"

#include <SFML/System/Vector2.hpp>
#include <imgui/imgui.h>
#include <imgui/imgui-SFML.h>
#include "../include/WindowLayout.h"


const static std::string static_enum_conversion[]
{
	u8"Iniciativa",
	u8"Reactiva  "
};

const static std::string line_pos_static_enum_conversion[]
{
	u8"Iniciativa",
	u8"Reactiva",
	u8"Punto medio"
};

const static std::string line_type_static_enum_conversion[]
{
	u8"Continua",
	u8"Discontinua",
	u8"Borde"
};

const std::string & action_enum_to_string(const ACTION_ENUM action)
{
	return static_enum_conversion[action];
}

const ACTION_ENUM action_string_to_enum(const std::string & action)
{
	{
		for (int i = 0; i < ACTION_ENUM::AE_COUNT; ++i)
		{
			if (static_enum_conversion[i] == action)
			{
				return (ACTION_ENUM)i;
			}
		}
		// should never be triggered
		throw "Invalid aciton enum on action_string_to_enum conversion";
		return ACTION_ENUM::AE_INICIATIVE;
	}
}

const std::string & line_type_enum_to_string(const LINE_TYPE_ENUM action)
{
	return line_type_static_enum_conversion[action];
}

const LINE_TYPE_ENUM line_type_string_to_enum(const std::string & action)
{
	for (int i = 0; i < LINE_TYPE_ENUM::LTE_COUNT; ++i)
	{
		if (line_type_static_enum_conversion[i] == action)
		{
			return (LINE_TYPE_ENUM)i;
		}
	}
	// should never be triggered
	throw "Invalid aciton enum on action_string_to_enum conversion";
	return LINE_TYPE_ENUM::LTE_CONSTANT;
}

const std::string & line_pos_enum_to_string(const LINE_POS_ENUM action)
{
	return line_pos_static_enum_conversion[action];
}

const LINE_POS_ENUM line_pos_string_to_enum(const std::string & action)
{
	for (int i = 0; i < LINE_POS_ENUM::LPE_COUNT; ++i)
	{
		if (line_pos_static_enum_conversion[i] == action)
		{
			return (LINE_POS_ENUM)i;
		}
	}
	// should never be triggered
	throw "Invalid aciton enum on action_string_to_enum conversion";
	return LINE_POS_ENUM::LPE_MIDDPOINT;
}

void ACTION_FIGURE_DATA::pass_figure_data_to_main_window()
{
	static Instance& main_instance = Instance::inst();

	// give the figure data to the main window, so we can evaluate what 
	// type of figure we are working on
	for (auto&& window : main_instance.m_all_windows) // could be array using the enums as index... 
	{
		if (window->m_window_type == WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT)
		{
			static_cast<WINDOW_MAIN_EDIT_TEXT*>(window)->add_figure_to_world(this);
			break;
		}
	}
}

void FIGURE_ACTIONS::add_new_action(ACTION * new_action)
{
	if (m_actions.size() > 0)
	{
		if (new_action->m_type == ACTION_ENUM::AE_REACTIVE)
		{
			if (m_actions.back()->m_type == ACTION_ENUM::AE_INICIATIVE)
			{
				m_actions.back()->m_next_link = new_action;
				new_action->m_previous_link = m_actions.back();
			}
		}
		else
		{
			if (m_actions.back()->m_type == ACTION_ENUM::AE_REACTIVE)
			{
				m_actions.back()->m_next_link = new_action;
				new_action->m_previous_link = new_action;
			}
		}
	}
	new_action->m_parent = this;
	m_actions.push_back(new_action);
}

void FIGURE_ACTIONS::draw(const int level)
{
	m_min_height = std::numeric_limits<float>().max();
	m_max_height = std::numeric_limits<float>().min();

	for (auto&& action : m_actions)
	{
		m_min_height = (action->m_min_height < m_min_height) ? action->m_min_height : m_min_height;
		m_max_height = (action->m_max_height > m_max_height) ? action->m_max_height : m_max_height;
		action->draw(level);
	}
}

void FIGURE_ACTIONS::save(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement& element_doc)
{
	for (auto&& actions : m_actions) 
	{
		auto&& parent_node = doc.NewElement("action");
		element_doc.InsertEndChild(parent_node);
		actions->save(doc, *parent_node);
	}
	if (m_childs.size() > 0) 
	{		
		for (auto&& child : m_childs)
		{
			auto&& child_node = doc.NewElement("childs");
			element_doc.InsertEndChild(child_node);

			child_node->SetAttribute("Figure_name", child->m_actions_data.m_name.c_str());
			child_node->SetAttribute("Figure_id", child->m_id);
			child_node->SetAttribute("Parent_id", (child->m_parent) ? child->m_parent->m_id : -1);
			child->save(doc, *child_node);
		}
	}	
}

void FIGURE_ACTIONS::load(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement& element_doc)
{
	for (auto&& actions : m_actions)
	{
		actions->load(doc,element_doc);
	}
}



float determinant(const float x1, const float y1, const float x2, const float y2)
{
	return (x1 * y2) - (x2 * y1);
}

bool intersection_point(
	const sf::Vector2f& line1_point1,
	const sf::Vector2f& line1_point2,
	const sf::Vector2f&line2_point1,
	const sf::Vector2f& line2_point2,
	sf::Vector2f& out_point)
{
	const float x1 = line1_point1.x;
	const float x2 = line1_point2.x;
	const float x3 = line2_point1.x;
	const float x4 = line2_point2.x;

	const float y1 = line1_point1.y;
	const float y2 = line1_point2.y;
	const float y3 = line2_point1.y;
	const float y4 = line2_point2.y;

	const float numerator1 = (determinant(x1, y1, x2, y2)	 * determinant(x3, 1.0, x4, 1.0)) - (determinant(x1, 1.0, x2, 1.0) * determinant(x3, y3, x4, y4));
	const float denominator1 = (determinant(x1, 1.0, x2, 1.0) * determinant(y3, 1.0, y4, 1.0)) - (determinant(y1, 1.0, y2, 1.0) * determinant(x3, 1.0, x4, 1.0));
	const float intersected_x = numerator1 / (denominator1 + 0.00001f);

	const float numerator2 = (determinant(x1, y1, x2, y2)	 * determinant(y3, 1.0, y4, 1.0)) - (determinant(y1, 1.0, y2, 1.0) * determinant(x3, y3, x4, y4));
	const float denominator2 = (determinant(x1, 1.0, x2, 1.0) * determinant(y3, 1.0, y4, 1.0)) - (determinant(y1, 1.0, y2, 1.0) * determinant(x3, 1.0, x4, 1.0));
	const float intersected_y = numerator2 / (denominator2 + 0.00001f);

	out_point = sf::Vector2f(intersected_x, intersected_y);

	const sf::Vector2f to = line2_point1 - out_point;
	const sf::Vector2f to2 = line2_point1 - line2_point2;
	const float dist_sq1 = (to.x * to.x + to.y * to.y);
	const float dist_sq2 = (to2.x * to2.x + to2.y * to2.y);

	return out_point != sf::Vector2f(0.0f, 0.0f) && dist_sq1 < dist_sq2;

	//return out_point != sf::Vector2f(0.0f,0.0f);
}

static sf::Vector2f offset(-20.0f, 5.0f);
static float end_offset = 50.0f;
void OUT_ACTION::draw(const int level)
{
	static Instance& main_instance = Instance::inst();
	const int current_width = main_instance.m_window_size.x;
	const int limit_width = (current_width * 0.1f);
	const int max_levels = 5;
	const int fraction = limit_width / max_levels;
	const int current_level = (fraction * level) + end_offset;
	m_level_hierarchy = level;

	// draw only half of the "figure"
	const sf::Vector2f init = m_origin.m_text_global_pos + offset;
	if (m_next_link != nullptr)
	{
		const sf::Vector2f end = m_next_link->m_origin.m_text_global_pos + offset;
		sf::Vector2f mid;
		if (m_line_pos == LINE_POS_ENUM::LPE_MIDDPOINT)
		{
			mid = sf::Vector2f(current_level, (init.y + end.y) * 0.5f);
		}
		else if (m_line_pos == LINE_POS_ENUM::LPE_INIT)
		{
			mid = sf::Vector2f(current_level, init.y);
		}
		else if (m_line_pos == LINE_POS_ENUM::LPE_REACTIVE)
		{
			mid = sf::Vector2f(current_level, end.y);
		}

		if (m_parent != nullptr)
		{
			if (m_parent->m_parent != nullptr)
			{
				for (auto&& parent_point : m_parent->m_parent->m_actions)
				{
					const sf::Vector2f init_parent = parent_point->m_origin.m_text_global_pos + offset;
					if (parent_point->m_next_link != nullptr)
					{
						const sf::Vector2f end_parent = parent_point->m_next_link->m_origin.m_text_global_pos + offset;

						sf::Vector2f mid_parent;
						if (parent_point->m_next_link->m_line_pos == LINE_POS_ENUM::LPE_MIDDPOINT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, (init_parent.y + end_parent.y) * 0.5f);
						}
						else if (parent_point->m_next_link->m_line_pos == LINE_POS_ENUM::LPE_INIT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, init_parent.y);
						}
						else if (parent_point->m_next_link->m_line_pos == LINE_POS_ENUM::LPE_REACTIVE)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, end_parent.y);
						}


						sf::Vector2f intersection;
						if (intersection_point(init_parent, mid_parent, init, mid, intersection))
						{
							m_intersection_point = intersection;
							mid = intersection;
						};
					}
				}
				for (auto&& rev_itr = m_parent->m_parent->m_actions.rbegin(); rev_itr != m_parent->m_parent->m_actions.rend(); ++rev_itr)
				{
					auto&& parent_point = *rev_itr;
					if (parent_point == nullptr) { continue; }
					if (parent_point == nullptr) {}
					const sf::Vector2f init_parent = parent_point->m_origin.m_text_global_pos + offset;
					if (parent_point->m_previous_link != nullptr)
					{
						const sf::Vector2f end_parent = parent_point->m_previous_link->m_origin.m_text_global_pos + offset;
						sf::Vector2f mid_parent;
						if (parent_point->m_previous_link->m_line_pos == LINE_POS_ENUM::LPE_MIDDPOINT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, (init_parent.y + end_parent.y) * 0.5f);
						}
						else if (parent_point->m_previous_link->m_line_pos == LINE_POS_ENUM::LPE_INIT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, init_parent.y);
						}
						else if (parent_point->m_previous_link->m_line_pos == LINE_POS_ENUM::LPE_REACTIVE)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, end_parent.y);
						}


						sf::Vector2f intersection;
						if (intersection_point(init_parent, mid_parent, init, mid, intersection))
						{
							m_intersection_point = intersection;
							mid = intersection;
						};
					}
				}
			}
		}
		set_min_max_height(init, mid);
		draw_line(init, mid, m_origin.m_participant_id);
	}
	else
	{
		set_min_max_height(init, init);
		draw_line(init, sf::Vector2f(current_level, init.y), m_origin.m_participant_id);
	}
}

void IN_ACTION::draw(const int level)
{
	// draw only half of the "figure"
	static Instance& main_instance = Instance::inst();
	const int current_width = main_instance.m_window_size.x;
	const int limit_width = current_width * 0.1f;
	const int max_levels = 5;
	const int fraction = limit_width / max_levels;
	const int current_level = (fraction * level) + end_offset;
	m_level_hierarchy = level;

	const sf::Vector2f init = m_origin.m_text_global_pos + offset;
	if (m_previous_link != nullptr)
	{
		const sf::Vector2f init = m_origin.m_text_global_pos + offset;
		const sf::Vector2f end = m_previous_link->m_origin.m_text_global_pos + offset;
		sf::Vector2f mid;
		if (m_line_pos == LINE_POS_ENUM::LPE_MIDDPOINT)
		{
			mid = sf::Vector2f(current_level, (init.y + end.y) * 0.5f);
		}
		else if (m_line_pos == LINE_POS_ENUM::LPE_INIT)
		{
			mid = sf::Vector2f(current_level, end.y);
		}
		else if (m_line_pos == LINE_POS_ENUM::LPE_REACTIVE)
		{
			mid = sf::Vector2f(current_level, init.y);
		}

		if (m_parent != nullptr)
		{
			if (m_parent->m_parent != nullptr)
			{
				for (auto&& parent_point : m_parent->m_parent->m_actions)
				{
					const sf::Vector2f init_parent = parent_point->m_origin.m_text_global_pos + offset;
					if (parent_point->m_next_link != nullptr)
					{
						const sf::Vector2f end_parent = parent_point->m_next_link->m_origin.m_text_global_pos + offset;
						sf::Vector2f mid_parent;
						if (parent_point->m_next_link->m_line_pos == LINE_POS_ENUM::LPE_MIDDPOINT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, (init_parent.y + end_parent.y) * 0.5f);
						}
						else if (parent_point->m_next_link->m_line_pos == LINE_POS_ENUM::LPE_INIT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, init_parent.y);
						}
						else if (parent_point->m_next_link->m_line_pos == LINE_POS_ENUM::LPE_REACTIVE)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, end_parent.y);
						}
						sf::Vector2f intersection;
						if (intersection_point(init_parent, mid_parent, init, mid, intersection))
						{
							m_intersection_point = intersection;
							mid = intersection;
							if (m_previous_link)
							{
								mid = m_previous_link->m_intersection_point;
							}
						};
					}
				}
				for (auto&& rev_itr = m_parent->m_parent->m_actions.rbegin(); rev_itr != m_parent->m_parent->m_actions.rend(); ++rev_itr)
				{
					auto&& parent_point = *rev_itr;
					if (parent_point == nullptr) { continue; }
					if (parent_point == nullptr) {}
					const sf::Vector2f init_parent = parent_point->m_origin.m_text_global_pos + offset;
					if (parent_point->m_previous_link != nullptr)
					{
						const sf::Vector2f end_parent = parent_point->m_previous_link->m_origin.m_text_global_pos + offset;
						sf::Vector2f mid_parent;
						if (parent_point->m_previous_link->m_line_pos == LINE_POS_ENUM::LPE_MIDDPOINT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, (init_parent.y + end_parent.y) * 0.5f);
						}
						else if (parent_point->m_previous_link->m_line_pos == LINE_POS_ENUM::LPE_INIT)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, init_parent.y);
						}
						else if (parent_point->m_previous_link->m_line_pos == LINE_POS_ENUM::LPE_REACTIVE)
						{
							mid_parent = sf::Vector2f((fraction * std::max(0, level - 1)) + end_offset, end_parent.y);
						}
						sf::Vector2f intersection;
						if (intersection_point(init_parent, mid_parent, init, mid, intersection))
						{
							m_intersection_point = intersection;
							mid = intersection;
							if (m_previous_link)
							{
								mid = m_previous_link->m_intersection_point;
							}
						};
					}
				}
			}
		}
		set_min_max_height(init, mid);
		draw_line(init, mid, m_origin.m_participant_id);
	}
	else
	{
		set_min_max_height(init, init);
		draw_line(init, sf::Vector2f(current_level, init.y), m_origin.m_participant_id);
	}
}


std::vector<sf::Vector2f> ACTION::non_continous_line(const sf::Vector2f & init, const sf::Vector2f & end, const float divisions)
{
	std::vector<sf::Vector2f> line_points;	
	const sf::Vector2f direction = (end - init);
	float modulus = (sqrtf((direction.x * direction.x) + (direction.y * direction.y)) + 0.0001f);
	sf::Vector2f normalized_direction = (direction) / modulus;
	const float step = modulus / divisions;
	

	for (int i = 0; i < divisions; ++i) 
	{
		line_points.push_back(sf::Vector2f(init + (normalized_direction *  step * (float)i)));
	}

	return std::move(line_points);
}

void ACTION::draw_line(const sf::Vector2f & init, const sf::Vector2f & end, int id)
{
	static Instance& main_instance = Instance::inst();
		
	sf::Color colour = main_instance.m_colour_profile.m_colours[id];

	if (m_line_type == LINE_TYPE_ENUM::LTE_CONSTANT) 
	{
		ImGui::DrawLine(init, end, colour, 3.0f);
	}
	else if(m_line_type == LINE_TYPE_ENUM::LTE_NON_CONSTANT)
	{		
		auto&& points = non_continous_line(init, end, 10.0f);
		for (int i = 0; i < points.size() - 1; i+=2) 
		{
			ImGui::DrawLine(points[i], points[i + 1], colour, 3.0f);
		}
	}
	else if (m_line_type == LINE_TYPE_ENUM::LTE_BORDER)
	{
		ImGui::DrawLine(init, end, sf::Color(0, 255, 0, 255),5.0f);
		auto&& points = non_continous_line(init, end, 10.0f);
		for (int i = 0; i < points.size() - 1; i += 2)
		{
			ImGui::DrawLine(points[i], points[i + 1], colour,3.0f);
		}
	}
}

void ACTION::set_min_max_height(const sf::Vector2f& init, const sf::Vector2f& end)
{
	m_min_height = std::numeric_limits<float>().max();
	m_max_height = std::numeric_limits<float>().min();

	m_min_height = std::min(init.y, std::min(m_min_height, end.y));
	m_max_height = std::max(init.y, std::max(m_min_height, end.y));
}

void ACTION::save(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement& element_doc)
{
	
	auto&& element_line_type = doc.NewElement("line_type");
	auto&& element_line_pos = doc.NewElement("line_pos");
	auto&& element_type = doc.NewElement("type");
	auto&& origin = doc.NewElement("origin");

	element_doc.InsertEndChild(element_line_type);
	element_doc.InsertEndChild(element_line_pos);
	element_doc.InsertEndChild(element_type);
	element_doc.InsertEndChild(origin);

	element_line_type->SetAttribute("line_type",m_line_type);
	element_line_pos->SetAttribute("line_pos", m_line_pos);
	element_type->SetAttribute("type", m_type);
	origin->SetAttribute("origin", m_origin.internal_id);

}

void ACTION::load(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement& element_doc)
{

}
