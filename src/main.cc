#include <Windows.h>
#include <imgui/imgui.h>

#include <imgui/imgui-SFML.h>

#include <imguifilesystem/imguifilesystem.h>

#include <tinyxml2.h>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include "../include/Figures.h"
#include "../include/WindowLayout.h"

#include <codecvt>

#include "soloud.h"

#include "soloud_wav.h"

/*
	//BIG TODO, LINEAS CONTINUAS Y DISCONTINUAS BIEN
	///INTERSECCION CON EL PADRE HACIENDO EL TRAZADO BIEN, IGUAL QUE EL HIJO DE PUNTO MEDIO INIT REACTIVE BLAH BLAH BLAH...
	//CLASE LINEA PARA PINTAR, ESTARIA MEJOR??
	AUDIO, LINEA CON EL AUDIO
	//LINEAS GRUESAS CON DISCONTINUA DENTRO
	//EDITAR LAS LINEAS
	//EDITAR TEXTO
	GUARDAR CARGAR
	LOS IDS Y EL SORT PARA ESAS IDS
	ANTS DE IRSE, SUBIRLO PRIVADO A GITHUB Y CUANDO ESTE, PUBLICARLO EN PROTO

*/



using namespace sf;
using namespace tinyxml2;

// Convert a wide Unicode string to an UTF8 string
std::string utf8_encode(const std::wstring &wstr)
{
	if (wstr.empty()) return std::string();
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed, 0);
	WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
	return strTo;
}

void initStyle() {
	ImGuiStyle * style = &ImGui::GetStyle();

	style->Colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
	style->Colors[ImGuiCol_PopupBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
	style->Colors[ImGuiCol_Border] = ImVec4(0.80f, 0.80f, 0.83f, 0.88f);
	style->Colors[ImGuiCol_BorderShadow] = ImVec4(0.92f, 0.91f, 0.88f, 0.00f);
	style->Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_TitleBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.98f, 0.95f, 0.75f);
	style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
	style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
	style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	//style->Colors[ImGuiCol_ComboBg] = ImVec4(0.19f, 0.18f, 0.21f, 1.00f);
	style->Colors[ImGuiCol_CheckMark] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
	style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
	style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_Header] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_Column] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_ColumnHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style->Colors[ImGuiCol_ColumnActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_ResizeGrip] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	style->Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	//style->Colors[ImGuiCol_CloseButton] = ImVec4(0.40f, 0.39f, 0.38f, 0.16f);
	//style->Colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.40f, 0.39f, 0.38f, 0.39f);
	//style->Colors[ImGuiCol_CloseButtonActive] = ImVec4(0.40f, 0.39f, 0.38f, 1.00f);
	style->Colors[ImGuiCol_PlotLines] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
	style->Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
	style->Colors[ImGuiCol_PlotHistogram] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
	style->Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
	style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.25f, 1.00f, 0.00f, 0.43f);
	style->Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(1.00f, 0.98f, 0.95f, 0.73f);
}


int main()
{
	sf::RenderWindow window(sf::VideoMode(1280, 720), "Demo", sf::Style::Close);
	window.setFramerateLimit(60);
	ImGui::SFML::Init(window);
	
	initStyle();
	
	Instance& main_instance = Instance::inst();

	WINDOW_MAIN_EDIT_TEXT window_main_edit_text;	
	//window_main_edit_text.set_entries(parse_file("../../FILES/test.eaf"));
	window_main_edit_text.set_entries(std::vector<ENTRY>());
	window_main_edit_text.set_window_pos(0,20);
	window_main_edit_text.set_window_size(840,340);
	main_instance.add_new_window(&window_main_edit_text);

	WINDOW_ADD_FIGURES window_add_figures;
	window_add_figures.set_window_size(440,320);
	window_add_figures.set_window_pos(840,80);
	main_instance.add_new_window(&window_add_figures);
	
	WINDOW_AUDIO window_audio;
	window_audio.set_window_size(440,60);
	window_audio.set_window_pos(840,20);
	main_instance.add_new_window(&window_audio);
	
	WINDOW_MAIN_MENU window_main_menu;
	window_main_menu.set_window_size(440,40);
	window_main_menu.set_window_pos(0,0);
	main_instance.add_new_window(&window_main_menu);

	WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG window_popup_figure_config;
	main_instance.add_new_window(&window_popup_figure_config);

	WINDOW_POPUP_ADD_FIGURE window_pop_up_add_figure;
	main_instance.add_new_window(&window_pop_up_add_figure);

	WINDOW_POPUP_EDIT_ENTRIES window_pop_up_edit_entries;
	main_instance.add_new_window(&window_pop_up_edit_entries);

	WINDOW_EDIT_FIGURES window_edit_figures;
	window_edit_figures.set_window_size(440, 320);
	window_edit_figures.set_window_pos(840, 400);
	main_instance.add_new_window(&window_edit_figures);
	
	WINDOW_FILE_MANAGER window_file_manager;	
	main_instance.add_new_window(&window_file_manager);


	
	// initialize SoLoud.
	main_instance.soloud.init();
	
	main_instance.set_audio("../../audios/test.wav");
	//main_instance.play_audio();

	sf::Clock deltaClock;
	bool selected = false;
		

	/*{
		while (window.isOpen()) {
			sf::Event event;
			while (window.pollEvent(event)) {
				ImGui::SFML::ProcessEvent(event);

				if (event.type == sf::Event::Closed) {
					window.close();
				}
			}
			mousePosition = ImGui::GetMousePos();
			imguiIO = ImGui::GetIO();

			ImGui::SFML::Update(window, deltaClock.restart());

			windowSize.y = window.getSize().y;
			windowSize.x = window.getSize().x;

			scene.timer += ImGui::GetIO().DeltaTime;

			Vector2u size = (Vector2u(uint32_t(windowSize.x * 0.75f), 35));
			Vector2f pos(0, 0);
			defaultArgs = ImVec2(185.0, 0.0f);
			ImGui::SetNextWindowPos(pos);
			ImGui::SetNextWindowSize(size);
			if (ImGui::Begin("##MainProgressBar", 0, mainImguiMenuFlags | ImGuiWindowFlags_::ImGuiWindowFlags_NoScrollbar)) {
				if (imguiIO.MouseDown[0]) {
					if (mousePosition.y < 30.0f && mousePosition.x < 185.0f)
						scene.originX = mousePosition.x - 0.8f;
					mousePercentOverWindow = scene.originX / (185.0f);
				}
				ImGui::ProgressBar(mousePercentOverWindow, defaultArgs, "##NoText");
				ImGui::SameLine();
				if (ImGui::Button((scene.showGlobalLine) ? "Ocultar" : "Mostrar")) {
					scene.showGlobalLine = !scene.showGlobalLine;
				}
				ImGui::SameLine();

				if (scene.editingFigure) {
					scene.editingFigure->ediMainMenu(&scene);
					ImGui::SameLine();
				}
				else {
					switch (scene.addingNewFigure)
					{
					case kChoosenFigure_Pico:
						Figure::MainMenuBarInfo<Pico>(&scene);
						break;
					case kChoosenFigure_Independiente:
						Figure::MainMenuBarInfo<Independiente>(&scene);
						break;
					case kChoosenFigure_Peine:
						Figure::MainMenuBarInfo<Peine>(&scene);
						break;
					case kChoosenFigure_Cremallera:
						Figure::MainMenuBarInfo<Cremallera>(&scene);
						break;
					case kChoosenFigure_Rastrillo:
						Figure::MainMenuBarInfo<Rastrillo>(&scene);
						break;
					case kChoosenFigure_MAX:
						//None
						break;
					}
				}
			}

			ImGui::SetCursorPosX(ImGui::GetWindowWidth() * 0.75f);
			ImGui::Text((scene.orderByID) ? "Usando Intervenciones" : "Usando tiempos");
			ImGui::SameLine();
			ImGui::SetCursorPosX(ImGui::GetWindowWidth() * 0.95f);
			ImGui::ToggleButton("", &scene.orderByID);
			if (ImGui::IsItemClicked) {
				if (!scene.orderByID) {
					std::sort(scene.allEntriesForImgui.begin(), scene.allEntriesForImgui.end(), [&](const Cell<Entry>* a, const Cell<Entry>* b) {
						return a->data.times.x < b->data.times.x;
					});
					scene.textOrder = RenderTextOrder::kRenderTextOrder_time;
				};
				if (scene.orderByID) {
					scene.textOrder = RenderTextOrder::kRenderTextOrder_id;
					std::sort(scene.allEntriesForImgui.begin(), scene.allEntriesForImgui.end(), [&](const Cell<Entry>* a, const Cell<Entry>* b) {
						return a->id < b->id;
					});
				};
			}

			ImGui::End();


			size = (Vector2u(uint32_t(windowSize.x * 0.75f), windowSize.y));
			pos = Vector2f(0, 40);

			ImGui::SetNextWindowPos(pos);
			ImGui::SetNextWindowSize(size);

			Vector2f origin;
			Vector2f target;

			Vector2f MIDD_origin;
			Vector2f MIDD_target;


			if (ImGui::Begin("##MainWindow", 0, mainImguiMenuFlags)) {


				ImGui::SetCursorPos(ImVec2(8, 8));

				origin = Vector2f(scene.originX, 8.0f);
				target = Vector2f(scene.originX, scene.allEntriesForImgui.back()->cursorPosition.y);

				//Pass to add all text and positions, JUST TEXT
				{

					uint32_t counter = 0;
					std::string lastParticipant = "ZZZ";
					for (Cell<Entry>* entry : scene.allEntriesForImgui) {
						if (lastParticipant != entry->data.participant) {
							ImGui::Separator();
							ImGui::SetCursorPosX(200.0f);
							ImGui::Text((entry->data.participant).c_str());
							ImGui::SameLine();
							ImGui::SetCursorPosX(ImGui::GetWindowWidth() *0.75f);
							ImGui::Text(std::to_string(entry->data.times.x).c_str());

							lastParticipant = entry->data.participant;
							ImGui::Separator();
						}
						ImGui::SetCursorPosX(200.0f);
						entry->cursorPosition = ImGui::GetCursorPos();
						std::string debugText = u8"- " + std::to_string(counter++) + " : " + entry->data.text;// +" : " + std::to_string(entry.data.times.x);
						ImGui::Text(debugText.c_str());
						if (ImGui::IsItemHovered()) {
							ImGui::BeginTooltip();

							float hours = (int(entry->data.times.x * 0.000277777f) % 24);
							float minutes = (int(entry->data.times.x * 0.016666f) % 60);
							float secs = (int(entry->data.times.x) % 60);

							ImGui::Text(entry->data.text.c_str());
							std::string tmpString = u8"Tiempos: " + std::to_string(entry->data.times.x) + "'s a " + std::to_string(entry->data.times.y) + "'s";
							ImGui::Text(tmpString.c_str());
							tmpString = u8"Id: " + std::to_string(entry->id);
							ImGui::Text(tmpString.c_str());
							tmpString = u8"Hora: " + std::to_string(hours) + " Minuto: " + std::to_string(minutes) + " Segundo: " + std::to_string(secs);
							ImGui::Text(tmpString.c_str());
							tmpString = u8"Participante ID: " + entry->data.participant;
							ImGui::Text(tmpString.c_str());
							tmpString = u8"ID: " + entry->data.annotationID;
							ImGui::Text(tmpString.c_str());
							ImGui::EndTooltip();
						};

					}
					ImGui::Text("");
					ImGui::Text("");
					ImGui::Text("");
					ImGui::Text("");

					//pass to add only debug position
					for (const auto v : scene.allEntriesForImgui) {
						//ImGui::SetCursorPos(ImVec2(800.0, v.cursorPosition.y));
						//ImGui::Text("%f %f", v.cursorPosition.x, v.cursorPosition.y);
					}

					if (scene.showImguiNames) {
						for (const auto& r : scene.allFigures) {
							r->printImguiName(&scene);
						}
					}



					if (scene.textOrder == RenderTextOrder::kRenderTextOrder_id) {
						ImGui::PushItemWidth(32);
						for (Cell<Entry>* entry : scene.allEntriesForImgui) {
							ImGui::SetCursorPos(ImVec2(10.0f, entry->cursorPosition.y - 4));
							lastParticipant = "##" + std::to_string(uint32_t(entry));
							int lastID = entry->id;
							if (ImGui::InputInt(lastParticipant.c_str(), &entry->id, 0, 0, ImGuiInputTextFlags_::ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_::ImGuiInputTextFlags_CharsNoBlank)) {

								if (lastID < entry->id) {
									for (uint32_t i = lastID + 1; i < entry->id + 1; ++i) {
										--scene.allEntriesForImgui[i]->id;
									}
								}
								else {
									for (uint32_t i = entry->id; i < lastID; ++i) {
										++scene.allEntriesForImgui[i]->id;
									}
								}

								std::sort(scene.allEntriesForImgui.begin(), scene.allEntriesForImgui.end(), [&, entry](const Cell<Entry>* a, const Cell<Entry>* b) {
									return a->id < b->id;
								});
								break;
							};
						}
						ImGui::PopItemWidth();
					}

					ImGui::SetCursorPos(ImVec2(8, 42));

					MIDD_origin = Vector2f(185.0f, 0);
					MIDD_target = Vector2f(185.0f, scene.allEntriesForImgui.back()->cursorPosition.y);
					ImGui::DrawLine(MIDD_origin, MIDD_target, Color(128, 128, 128, 255));


				}

				if (scene.editingFigure) {
					scene.editingFigure->editfigure(&scene);
				}
				else {
					switch (scene.addingNewFigure)
					{
					case kChoosenFigure_Pico:
						Figure::toAddModde<Pico>(&scene);
						break;
					case kChoosenFigure_Independiente:
						Figure::toAddModde<Independiente>(&scene);
						break;
					case kChoosenFigure_Peine:
						Figure::toAddModde<Peine>(&scene);
						break;
					case kChoosenFigure_Cremallera:
						Figure::toAddModde<Cremallera>(&scene);
						break;
					case kChoosenFigure_Rastrillo:
						Figure::toAddModde<Rastrillo>(&scene);
						break;
					case kChoosenFigure_MAX:
						//None
						break;
					}
				}


				scene.draw();


			};
			ImGui::End();


			size = (Vector2u(uint32_t(windowSize.x * 0.25f), uint32_t(windowSize.y * 0.75f)));
			pos = Vector2f(uint32_t(windowSize.x * 0.75f), 0.0f);

			ImGui::SetNextWindowPos(pos);
			ImGui::SetNextWindowSize(size);
			if (ImGui::Begin("##choosingFigures", 0, mainImguiMenuFlags | ImGuiWindowFlags_::ImGuiWindowFlags_NoScrollbar)) {
				if (ImGui::Button("Pico         ")) {
					scene.addingNewFigure = ChoosenFigure::kChoosenFigure_Pico;
					scene.editingFigure = nullptr;
				}
				if (ImGui::Button("Independiente")) {
					scene.addingNewFigure = ChoosenFigure::kChoosenFigure_Independiente;
					scene.editingFigure = nullptr;
				}
				if (ImGui::Button("Peine        ")) {
					scene.addingNewFigure = ChoosenFigure::kChoosenFigure_Peine;
					scene.editingFigure = nullptr;
				}
				if (ImGui::Button("Cremallera   ")) {
					scene.addingNewFigure = ChoosenFigure::kChoosenFigure_Cremallera;
					scene.editingFigure = nullptr;
				}
				if (ImGui::Button("Rastrillo    ")) {
					scene.addingNewFigure = ChoosenFigure::kChoosenFigure_Rastrillo;
					scene.editingFigure = nullptr;
				}
				if (ImGui::Button("Limpiar      ")) {
					scene.addingNewFigure = ChoosenFigure::kChoosenFigure_MAX;
					scene.editingFigure = nullptr;
				}

				ImGui::Text("");
				ImGui::Separator();
				ImGui::Text("");

				ImGui::Checkbox(u8"Mostrar nombres", &scene.showImguiNames);

				ImGui::Text("");
				ImGui::Separator();
				ImGui::Text("");

				scene.totalOfFigures[0] = 0;
				scene.totalOfFigures[1] = 0;
				scene.totalOfFigures[2] = 0;
				scene.totalOfFigures[3] = 0;
				scene.totalOfFigures[4] = 0;

				std::for_each(scene.allFigures.begin(), scene.allFigures.end(), [&](Figure* f) {
					f->ImguiName = f->name + std::to_string(scene.totalOfFigures[int(f->myType)]++);
				});

				ImGui::Text(u8"Estadísticas");
				ImGui::Text(u8"Total de picos:          %d", scene.totalOfFigures[ChoosenFigure::kChoosenFigure_Pico]);
				ImGui::Text(u8"Total de independientes: %d", scene.totalOfFigures[ChoosenFigure::kChoosenFigure_Independiente]);
				ImGui::Text(u8"Total de peines:         %d", scene.totalOfFigures[ChoosenFigure::kChoosenFigure_Peine]);
				ImGui::Text(u8"Total de Cremalleras:    %d", scene.totalOfFigures[ChoosenFigure::kChoosenFigure_Cremallera]);
				ImGui::Text(u8"Total de rastrillos:     %d", scene.totalOfFigures[ChoosenFigure::kChoosenFigure_Rastrillo]);

				ImGui::Text("");
				ImGui::Separator();
				ImGui::Text("");

				char aux[32];
				uint32_t counter2 = 0;
				for (auto& r : scene.allFigures) {
					ImGui::Separator();
					ImGui::Text(r->customName.c_str());
					ImGui::BulletText((r->ImguiName + " | Tiempo: " + std::to_string(r->getFirstEntry()->data.times.x)).c_str());
					r->hovered = ImGui::IsItemHovered();

					if (r->hovered) {
						if (ImGui::BeginPopupContextWindow())
						{
							if (ImGui::MenuItem("Custom", NULL)) {}
							if (ImGui::MenuItem("Top-left", NULL)) {}
							if (ImGui::MenuItem("Top-right", NULL)) {}
							if (ImGui::MenuItem("Bottom-left", NULL)) {}
							if (ImGui::MenuItem("Bottom-right", NULL)) {}
							ImGui::EndPopup();
						}
					}
					if (ImGui::IsItemClicked()) {
						scene.editingFigure = r;
					}
					ImGui::SameLine();
					sprintf_s(aux, 32, "Eliminar##%d", (uint32_t)&r);
					if (ImGui::SmallButton(aux)) {
						scene.allFigures.erase(scene.allFigures.begin() + counter2);
						break;
					};

					if (scene.editingFigure == r) {
						scene.editingFigure->editingLines(&scene);
					}

					++counter2;
				}


			}
			ImGui::End();


			size = (Vector2u(uint32_t(windowSize.x * 0.25f), uint32_t(windowSize.y * 0.25f)));
			pos = Vector2f(uint32_t(windowSize.x * 0.75f), uint32_t(windowSize.y * 0.75f));

			ImGui::SetNextWindowPos(pos);
			ImGui::SetNextWindowSize(size);
			if (ImGui::Begin("##choosingColors", 0, mainImguiMenuFlags | ImGuiWindowFlags_::ImGuiWindowFlags_NoScrollbar)) {
				static int participantID = 0;
				if (ImGui::DragInt("Participante", &participantID, 1.0f, 0, 9)) {
					participantID = min(9, participantID);
					participantID = max(0, participantID);
				};

				ImGui::ColorEdit3("Color", &scene.globalColores[participantID].x);
			}


			ImGui::End();


			window.clear();
			//window.draw(shape);
			ImGui::SFML::Render(window);
			window.display();
		}
	}*/
	{
		while (window.isOpen()) {
			sf::Event event;
			while (window.pollEvent(event)) {
				ImGui::SFML::ProcessEvent(event);

				if (event.type == sf::Event::Closed) {
					window.close();
				}
			}
			main_instance.m_window_size.x = (float)window.getSize().x;
			main_instance.m_window_size.y = (float)window.getSize().y;

			ImGui::SFML::Update(window, deltaClock.restart());
			
			for (auto& window : main_instance.m_all_windows) 
			{
				window->update();
			}
			

			window.clear();
			//window.draw(shape);		
			ImGui::SFML::Render(window);
			window.display();
		}
	}

	// Clean up SoLoud
	main_instance.soloud.deinit();

	ImGui::SFML::Shutdown();
	window.close();	
}


/*
 // Menu
	
*/