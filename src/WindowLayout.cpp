#include "..\include\WindowLayout.h"
#include "..\include\Figures.h"
#include <imgui/imgui-SFML.h>
#include <imguifilesystem.h>
#include <tinyxml2.h>
#include <functional>



static char input_text_notes[4096];
static char input_text_edit[4096];
static char buffer[4096];

std::string WINDOW_MAIN_MENU::m_relative_path = "../../DATA/";
std::string WINDOW_MAIN_MENU::m_default_file_figues_data = "figures.xml";
std::string WINDOW_MAIN_MENU::m_default_file_colour_profile = "colour_profiles.xml";
std::string WINDOW_MAIN_MENU::m_default_file_entries = "entries.xml";
std::string WINDOW_MAIN_MENU::m_default_file_web_solution = "web_based.xml";
std::string WINDOW_MAIN_MENU::m_default_file_figures_use = "figures_use.xml";
std::string WINDOW_MAIN_MENU::m_serialized_data = "serialized_scene.xml";

std::string WINDOW_MAIN_MENU::m_default_file_figues_data_string = "los datos de las figuras";
std::string WINDOW_MAIN_MENU::m_default_file_colour_profile_string = "los perfiles de color";
std::string WINDOW_MAIN_MENU::m_default_file_entries_string = "todas las entradas";
std::string WINDOW_MAIN_MENU::m_default_file_web_solution_string = "solucion web";
std::string WINDOW_MAIN_MENU::m_default_file_figures_use_string = "estadisticas sobre las figuras";
std::string WINDOW_MAIN_MENU::m_serialized_string = "programa entero o EAF";

void recursive_child(tinyxml2::XMLElement* entry_point, WINDOW_MAIN_EDIT_TEXT* main_window, Instance& main_instance);
void recursive_function(const tinyxml2::XMLElement* entry_point, WINDOW_MAIN_EDIT_TEXT* main_window, Instance& main_instance);

std::string WINDOW_MAIN_MENU::m_default_option_selected[6] =
{
	m_default_file_figues_data_string,
	m_default_file_colour_profile_string,
	m_default_file_entries_string,
	m_default_file_web_solution_string,
	m_default_file_figures_use_string,
	m_serialized_string
};

void WINDOW_LAYOUT::begin_window()
{
	ImVec2 pos((float)m_pos_x, (float)m_pos_y);
	ImVec2 size((float)m_size_x, (float)m_size_y);

	ImGui::SetNextWindowPos(pos);
	ImGui::SetNextWindowSize(size);

	ImGui::Begin(m_window_name.c_str(), (bool)0,
		ImGuiWindowFlags_::ImGuiWindowFlags_NoTitleBar | m_window_mask);

	reset_cursor();
}

void WINDOW_LAYOUT::end_window()
{
	ImGui::End();
}

void WINDOW_LAYOUT::reset_cursor()
{
	set_cursor(sf::Vector2f(0.0f, 0.0f));
}

void WINDOW_LAYOUT::set_cursor(const sf::Vector2f & cursor_pos)
{
	ImGui::SetCursorPos(cursor_pos);
}

void WINDOW_MAIN_EDIT_TEXT::update()
{
	if (!m_visible)
	{
		return;
	}
	static Instance& main_instance = Instance::inst();
	set_window_size((unsigned int)m_size_x, (unsigned int)(main_instance.m_window_size.y - 20));
	set_window_pos(0, 20);

	begin_window();

	if (m_data.m_all_entries.size() > 0)
	{

		//ImGui::DrawLine(ImVec2(0.0f, main_instance.m_audio_height_main_window), ImVec2(main_instance.m_window_size.x, main_instance.m_audio_height_main_window),sf::Color::White);

		m_data.m_max_heigh = m_data.m_all_entries.back().m_text_global_pos.y;
		main_instance.m_max_heigh = m_data.m_max_heigh;
		main_instance.m_max_time = m_data.m_all_entries.back().m_time.y;


		Vector2f top_screen_pos((float)m_data.m_horizontal_ofset * 0.9f, 0);
		Vector2f bottom_screen_pos((float)m_data.m_horizontal_ofset * 0.9f, std::prev(m_data.m_all_entries.end())->m_text_global_pos.y);
		ImGui::DrawLineAbsolute(top_screen_pos, bottom_screen_pos, sf::Color::White);

		ImGui::SetCursorPosY((float)m_data.m_vertical_ofset);

		int last_participant_id = 0;

		if (m_data.m_all_entries.size() > 0)
		{
			last_participant_id = m_data.m_all_entries.begin()->m_participant_id;
			ImGui::Separator();
			ImGui::SetCursorPosX((float)m_data.m_horizontal_ofset);
			ImGui::Text(m_data.m_all_entries.begin()->m_participant.c_str());
			ImGui::Separator();
		}


		for (auto&& entry : m_data.m_all_entries)
		{
			if (entry.m_participant_id != last_participant_id)
			{
				last_participant_id = entry.m_participant_id;
				ImGui::Separator();
				ImGui::SetCursorPosX((float)m_data.m_horizontal_ofset);
				ImGui::Text(entry.m_participant.c_str());
				ImGui::Separator();
			}
			ImGui::SetCursorPosX((float)m_data.m_horizontal_ofset);
			entry.m_text_global_pos = ImGui::GetCursorPos();

			const float current_sound_time = main_instance.m_audio_currentl_time;
			const float current_text_time_min = entry.m_time.x;
			const float current_text_time_max = entry.m_time.y;

			const bool is_on_time =
				(current_sound_time > current_text_time_min &&
					current_sound_time < current_text_time_max);
			float dist_to_time = ((current_text_time_max + current_text_time_min) * 0.5) - current_sound_time;
			const float dist_to_time_p = dist_to_time / 2.0f;
			const bool is_close = dist_to_time < 2.0f && dist_to_time < 0.0f;


			if (is_close)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(dist_to_time_p, dist_to_time_p, 1.0f, 1.0f));
			}
			if (is_on_time)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.8f, 0.8f, 0.0f, 1.0f));
			}

			sprintf(buffer, u8"%s ##%d", entry.m_text.c_str(), entry.internal_id);

			if (ImGui::Selectable(buffer))
			{
				main_instance.open_pop_up_window(WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_EDIT_ENTRIES);
				main_instance.set_edit_entry(&entry);
			}
			if (is_on_time)
			{
				ImGui::PopStyleColor(1);
			}
			if (is_close)
			{
				ImGui::PopStyleColor(1);
			}
			if (ImGui::IsItemHovered())
			{
				if (entry.notes.size() > 0)
				{
					ImGui::BeginTooltip();
					ImGui::Text(entry.notes.c_str());
					ImGui::EndTooltip();
				}
			}
			if (entry.notes.size() > 0)
			{
				ImGui::SameLine();
				ImGui::SetCursorPosX((float)m_data.m_horizontal_ofset - 15.0f);
				ImGui::Text("*");
			}
		}

		draw_all_figures();

		// if we are currently adding a new figure, show all the possible configurations
		if (m_data.m_adding_new_figure)
		{
			adding_new_figure_information();
		}

		if (m_data.m_scrolling_to_this_figure)
		{
			if (m_data.m_scrolling_to_this_figure_instance)
			{
				if (m_data.m_scrolling_to_this_figure_instance->m_actions.size() > 0)
				{
					const float current_scroll = ImGui::GetScrollY();
					const float target_scroll = m_data.m_scrolling_to_this_figure_instance->m_actions.front()->m_origin.m_text_global_pos.y;

					const float distance_difference = current_scroll - target_scroll;
					const float new_scroll_y = current_scroll - (distance_difference * 0.05f);
					ImGui::SetScrollY(new_scroll_y);
					if (abs(distance_difference) < 10.0f)
					{
						m_data.m_scrolling_to_this_figure = false;
						m_data.m_scrolling_to_this_figure_instance = nullptr;
					}
				}
			}
		}
	}
	end_window();
}

void WINDOW_MAIN_EDIT_TEXT::add_figure_to_world(ACTION_FIGURE_DATA* data)
{
	m_data.m_current_adding_action_data = data;
	//m_adding_new_figure = true;
	// create a new figure at the end of the vector to work with	
}

void WINDOW_MAIN_EDIT_TEXT::scroll_to_this_figure(FIGURE_ACTIONS* data)
{
	m_data.m_scrolling_to_this_figure = true;
	m_data.m_scrolling_to_this_figure_instance = data;
}

ENTRY & WINDOW_MAIN_EDIT_TEXT::get_entry_by_id(int id)
{
	for (auto&& entry : m_data.m_all_entries)
	{
		if (entry.internal_id == id)
		{
			return entry;
		}
	}
	return ENTRY();
}

void WINDOW_MAIN_EDIT_TEXT::adding_new_figure_information()
{
	static Instance& main_instance = Instance::inst();

	static int current_action_index = 0;
	static int current_sibling_index = 0;

	ImGui::SetCursorPos(ImVec2(2.0f, 2.0f));

	ACTION_FIGURE_DATA* edited_data = nullptr;
	if (m_data.m_current_adding_action_data->m_other_figures.size() > 0)
	{
		edited_data = m_data.m_current_adding_action_data->m_other_figures[current_sibling_index].get();
	}
	else
	{
		edited_data = m_data.m_current_adding_action_data;
	}

	// insert a toolttip with the current information being added
	ImGui::BeginTooltip();
	std::string current_tooltip = u8"Selecciona una " + action_enum_to_string(edited_data->m_actions[current_action_index]);
	ImGui::Text(current_tooltip.c_str());
	ImGui::EndTooltip();

	ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.24f, 0.23f, 0.29f, 1.00f));
	ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, ImVec4(0.5f, 0.5f, 0.5f, 1.00f));

	// TODO: needs code refactor... horrible to have this duplicated	
	if (m_data.m_current_adding_action_data->m_other_figures.size() > 0)
	{
		for (auto&& current_entry : m_data.m_all_entries)
		{
			// reset curspor pos, so I can use global position
			ImGui::SetCursorPos(current_entry.m_text_global_pos - Vector2f(45.0f, 0.0f));
			ImGui::RadioButton("", false);
			if (ImGui::IsItemClicked())
			{
				handle_creation_of_figure_sibling(current_entry, *m_data.m_current_adding_action_data, current_action_index, current_sibling_index);
				++current_action_index;
				// move to next action and clamp
				// here should perform a check, tolet the user know there are no more actions
				if (current_action_index == edited_data->m_actions.size())
				{
					++current_sibling_index;
					if (current_sibling_index >= m_data.m_current_adding_action_data->m_other_figures.size())
					{
						// auto_ generate this figure
						m_data.m_current_edited_figure->m_actions_data.m_references++;
						m_data.m_adding_new_figure = false;
						m_data.m_current_edited_figure = nullptr;
						main_instance.get_current_figures_instances().figure_to_create = nullptr;
						main_instance.mask_all(~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
						current_action_index = 0;
						current_sibling_index = 0;
						break;
					}
					else
					{
						current_action_index = 0;
					}
				};
			}
		}
	}
	else if (edited_data->m_recursive == false)
	{
		if (m_data.m_current_figure_enum == FIGURE_ENUM::FE_SIMPLE)
		{
			for (auto&& current_entry : m_data.m_all_entries)
			{
				// reset curspor pos, so I can use global position
				ImGui::SetCursorPos(current_entry.m_text_global_pos - Vector2f(45.0f, 0.0f));
				ImGui::RadioButton("", false);
				if (ImGui::IsItemClicked())
				{
					handle_creation_of_figure(current_entry, *edited_data, current_action_index);
					++current_action_index;
					// move to next action and clamp
					// here should perform a check, tolet the user know there are no more actions
					if (current_action_index == edited_data->m_actions.size())
					{
						// auto_ generate this figure
						m_data.m_current_edited_figure->m_actions_data.m_references++;
						m_data.m_adding_new_figure = false;
						m_data.m_current_edited_figure = nullptr;
						main_instance.get_current_figures_instances().figure_to_create = nullptr;
						main_instance.mask_all(~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
						current_action_index = 0;
						break;
					};
				}
			}
		}
		else
		{
			for (auto&& current_entry : m_data.m_all_entries)
			{

				if (current_entry.m_text_global_pos.y - 5.0f > m_data.m_current_edited_figure->m_parent->m_min_height &&
					current_entry.m_text_global_pos.y + 5.0f < m_data.m_current_edited_figure->m_parent->m_max_height)
				{
					// reset curspor pos, so I can use global position
					ImGui::SetCursorPos(current_entry.m_text_global_pos - Vector2f(45.0f, 0.0f));
					ImGui::RadioButton("", false);
					if (ImGui::IsItemClicked())
					{
						handle_creation_of_figure(current_entry, *edited_data, current_action_index);
						++current_action_index;
						// move to next action and clamp
						// here should perform a check, tolet the user know there are no more actions
						if (current_action_index == edited_data->m_actions.size())
						{
							// auto_ generate this figure
							m_data.m_current_edited_figure->m_actions_data.m_references++;
							m_data.m_adding_new_figure = false;
							m_data.m_current_edited_figure = nullptr;
							main_instance.get_current_figures_instances().figure_to_create = nullptr;
							main_instance.mask_all(~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
							current_action_index = 0;
							current_sibling_index = 0;
							break;
						};
					}
				}
			}
		}
	}
	else
	{
		for (auto&& current_entry : m_data.m_all_entries)
		{
			// reset curspor pos, so I can use global position
			ImGui::SetCursorPos(current_entry.m_text_global_pos - Vector2f(45.0f, 0.0f));
			ImGui::RadioButton("", false);
			if (ImGui::IsItemClicked())
			{
				handle_creation_of_figure(current_entry, *edited_data, current_action_index);

				//if (edited_data->m_actions[current_action_index] == ACTION_ENUM::AE_REACTIVE)
				//{
				//	if (current_action_index > 0) 
				//	{
				//		if (edited_data->m_actions[current_action_index - 1] == ACTION_ENUM::AE_INICIATIVE) 
				//		{
				//			handle_creation_of_figure(current_entry, *edited_data, current_action_index - 1);
				//			++current_action_index;
				//		}
				//	}
				//}
				++current_action_index;
				current_action_index = current_action_index % edited_data->m_actions.size();
				break;
			}
		}
	}
	/*
		m_adding_new_figure = false;
		m_current_edited_figure = nullptr;
		main_instance.figure_to_create = nullptr;
		main_instance.mask_all(~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
		current_action_index = 0;
	*/

	ImGui::PopStyleColor(2);

	// exit this mode
	if (ImGui::IsKeyDown(ImGui::GetKeyIndex(ImGuiKey_::ImGuiKey_Escape)))
	{
		m_data.m_current_edited_figure = nullptr;
		m_data.m_adding_new_figure = false;
		main_instance.get_current_figures_instances().figure_to_create = nullptr;
		current_action_index = 0;
		main_instance.mask_all(~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
		current_sibling_index = 0;



		//main_instance.m_all_figures.pop_back();
		//TODO: ONLY REMOVE THE LAST ADDED< NO THE LAST ONE, IT MIGHT BE A CHILD THE NEW ADDED FIGURE!!!
	}
}


void WINDOW_MAIN_EDIT_TEXT::handle_creation_of_figure(const ENTRY& new_entry, const ACTION_FIGURE_DATA& data, const int current_index)
{
	static Instance& main_instance = Instance::inst();

	auto&& action = data.m_actions[current_index];

	ACTION* new_action = nullptr;
	// when draw, if the sequence is OUT_ACTION then IN_ACTION, will automatically link
	switch (action)
	{
	case ACTION_ENUM::AE_INICIATIVE:
	{
		new_action = new OUT_ACTION(new_entry);
		break;
	}
	case ACTION_ENUM::AE_REACTIVE:
	{
		new_action = new IN_ACTION(new_entry);
		break;
	}
	default:
		break;
	}

	new_action->m_line_pos = data.m_line_pos[current_index];
	new_action->m_line_type = data.m_line_types[current_index];

	m_data.m_current_edited_figure->add_new_action(new_action);
}

void WINDOW_MAIN_EDIT_TEXT::handle_creation_of_figure_sibling(const ENTRY& new_entry, const ACTION_FIGURE_DATA& data, const int current_index, const int current_sibling_index)
{
	static Instance& main_instance = Instance::inst();

	auto&& action = data.m_other_figures[current_sibling_index]->m_actions[current_index];

	ACTION* new_action = nullptr;
	// when draw, if the sequence is OUT_ACTION then IN_ACTION, will automatically link
	switch (action)
	{
	case ACTION_ENUM::AE_INICIATIVE:
	{
		new_action = new OUT_ACTION(new_entry);
		break;
	}
	case ACTION_ENUM::AE_REACTIVE:
	{
		new_action = new IN_ACTION(new_entry);
		break;
	}
	default:
		break;
	}

	new_action->m_line_pos = data.m_other_figures[current_sibling_index]->m_line_pos[current_index];
	new_action->m_line_type = data.m_other_figures[current_sibling_index]->m_line_types[current_index];

	m_data.m_current_edited_figure->add_new_action(new_action);
}

void WINDOW_MAIN_EDIT_TEXT::draw_all_figures()
{
	static Instance& main_instance = Instance::inst();
	reset_cursor();

	for (auto&& current_figure : main_instance.get_current_figures_instances().m_all_figures)
	{
		draw_recursive_figures(current_figure, 0);
	}
}

void WINDOW_MAIN_EDIT_TEXT::draw_recursive_figures(std::shared_ptr<FIGURE_ACTIONS> figure_actions, const int level)
{
	figure_actions->draw(level);
	for (auto&& childs : figure_actions->m_childs)
	{
		draw_recursive_figures(childs, level + 1);
	}
}

void WINDOW_ADD_FIGURES::update()
{
	if (!m_visible)
	{
		return;
	}
	static Instance& main_instance = Instance::inst();

	begin_window();

	ImGui::Text(u8"					A�adir nuevas figuras");
	ImGui::Separator();
	ImGui::Text("");

	if (ImGui::Button(u8"Crear una nueva figura"))
	{
		main_instance.open_pop_up_window(WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG);
	};
	ImGui::Separator();
	ImGui::Separator();

	ImGui::Text("Figuras creadas:");
	for (auto&& figure : main_instance.m_all_actions_data)
	{
		if (ImGui::Button(figure->m_name.c_str()))
		{
			main_instance.open_pop_up_window(WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_ADD_FIGURE);
			figure->pass_figure_data_to_main_window();
		}
	}

	end_window();
}

// use the on in Figures.cpp
static const char* items_line_pos[] = { u8"Iniciativa", u8"Reactiva", u8"Punto medio" };
static const char* items_line_type[] = { u8"Continua", u8"Discontinua", u8"Borde" };
static char local_name_buffer_imgui_ids[512] = {};

void WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG::update()
{
	if (!m_visible)
	{
		return;
	}

	static Instance& main_instance = Instance::inst();

	set_window_pos((unsigned int)(main_instance.m_window_size.x * 0.2f), (unsigned int)(main_instance.m_window_size.y * 0.25f));
	set_window_size((unsigned int)(main_instance.m_window_size.x * 0.55f), (unsigned int)(main_instance.m_window_size.y * 0.55f));
	begin_window();

	if (ImGui::Button(u8"Salir"))
	{
		main_instance.return_input_all_windows();
		m_visible = false;
	};
	//------------------------------------------//

	static char local_name_buffer[512] = {};
	static bool local_add_sibling = false;

	static ACTION_FIGURE_DATA action_figure_data;
	ImGui::Text("");
	ImGui::Text(u8"Nombre de la figura");

	ImGui::PushItemWidth(m_size_x * 0.3f);
	sprintf(buffer, "##%d", &action_figure_data);
	ImGui::InputText(buffer, local_name_buffer, sizeof(char) * 512);

	if (ImGui::Checkbox(u8"�Formada por otras figuras?", &local_add_sibling))
	{
		if (!local_add_sibling)
		{
			action_figure_data.m_other_figures.clear();
		}
	};

	ImGui::Text("");
	ImGui::Text("");

	if (!local_add_sibling)
	{
		if (ImGui::Button(u8"�A�adir una acci�n iniciativa?"))
		{
			action_figure_data.m_actions.push_back(ACTION_ENUM::AE_INICIATIVE);
			action_figure_data.m_line_pos.resize(action_figure_data.m_actions.size());
			action_figure_data.m_line_types.resize(action_figure_data.m_actions.size());
		}
		if (ImGui::Button(u8"�A�adir una acci�n reactiva?"))
		{
			action_figure_data.m_actions.push_back(ACTION_ENUM::AE_REACTIVE);
			action_figure_data.m_line_pos.resize(action_figure_data.m_actions.size());
			action_figure_data.m_line_types.resize(action_figure_data.m_actions.size());
		}


		if (ImGui::Checkbox(u8"�Es una figura de repetici�n?", &action_figure_data.m_recursive))
		{

		};
	}
	else
	{
		ImGui::Text(u8"Posibles figuras hermanas: ");
		for (auto&& sibling_figure : main_instance.m_all_actions_data)
		{
			if (sibling_figure->m_recursive == true) { continue; }

			sprintf(buffer, "##%d", &sibling_figure);
			if (ImGui::RadioButton(buffer, true))
			{
				action_figure_data.m_other_figures.push_back(sibling_figure);
			}
			ImGui::SameLine();
			ImGui::Text(sibling_figure->m_name.c_str());
		}

		ImGui::Text(u8"Usandose ahora: ");
		for (auto&& sibling_figure : action_figure_data.m_other_figures)
		{
			ImGui::Text(sibling_figure->m_name.c_str());
		}
	}

	// set the cursor on the right half of the window to display all the added actions
	sf::Vector2f cursor_pos(m_size_x * 0.35f, 0.0f);
	set_cursor(cursor_pos);
	ImGui::DrawLine(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(0.0f, m_size_y), sf::Color::White, 1.0f);

	cursor_pos.x = 0.0f;
	cursor_pos.y = 20.0f;
	set_cursor(cursor_pos);

	//------------------------------------------//
	if (ImGui::Button(u8"Crear esta figura"))
	{
		if ((action_figure_data.m_actions.size() > 0 || (action_figure_data.m_other_figures.size() > 0)) &&
			strcmp(local_name_buffer, "") != 0)
		{
			action_figure_data.m_name = local_name_buffer;
			main_instance.create_new_data_figure(action_figure_data);
			memset(local_name_buffer, 0, sizeof(char) * 512);
			action_figure_data.clean();
		}
	};



	//---------------------------------------------//

	cursor_pos.x = m_size_x * 0.375f;
	cursor_pos.y = 20.0f;
	set_cursor(cursor_pos);
	ImGui::Text(u8"Click sobre una acci�n para eliminarla");

	cursor_pos.x = m_size_x * 0.375f;
	cursor_pos.y = 50.0f;
	set_cursor(cursor_pos);
	ImGui::Text(u8"Acciones");

	cursor_pos.x = m_size_x * 0.575f;
	cursor_pos.y = 50.0f;
	set_cursor(cursor_pos);
	ImGui::Text(u8"Enlace");

	cursor_pos.x = m_size_x * 0.775f;
	cursor_pos.y = 50.0f;
	set_cursor(cursor_pos);
	ImGui::Text(u8"Tipo de linea");

	const int amount_to_loop = action_figure_data.m_actions.size();
	for (int i = 0; i < amount_to_loop; ++i)
	{
		auto&& actions_added = action_figure_data.m_actions[i];
		cursor_pos.x = m_size_x * 0.375f;
		cursor_pos.y = 75.0f + (i * ImGui::GetFontSize() * 1.25f);
		set_cursor(cursor_pos);
		ImGui::Text(action_enum_to_string(actions_added).c_str());
		if (ImGui::IsItemClicked())
		{
			action_figure_data.m_actions.erase(action_figure_data.m_actions.begin() + i);
			action_figure_data.m_line_pos.resize(action_figure_data.m_actions.size());
			action_figure_data.m_line_types.resize(action_figure_data.m_actions.size());
			break;
		}

		ImGui::SameLine();
		sprintf_s(local_name_buffer_imgui_ids, sizeof(local_name_buffer_imgui_ids), "##linetypeCombo %d", i);
		ImGui::PushItemWidth(m_size_x * 0.25f);
		if (ImGui::BeginCombo(local_name_buffer_imgui_ids, line_type_enum_to_string(action_figure_data.m_line_types[i]).c_str()))
		{
			for (int n = 0; n < IM_ARRAYSIZE(items_line_type); n++)
			{
				bool is_selected = action_figure_data.m_line_types[i] == (LINE_TYPE_ENUM)n;
				if (ImGui::Selectable(items_line_type[n], is_selected))
				{
					action_figure_data.m_line_types[i] = (LINE_TYPE_ENUM)n;
				}
				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		if (i < amount_to_loop - 1)
		{
			if (actions_added == ACTION_ENUM::AE_INICIATIVE)
			{
				if (action_figure_data.m_actions[i + 1] == ACTION_ENUM::AE_REACTIVE)
				{
					// your pos, midd, other link
					ImGui::SameLine();
					cursor_pos.x = ImGui::GetCursorPosX();
					cursor_pos.y = 75.0f + (i * ImGui::GetFontSize() * 1.25f) + ((ImGui::GetFontSize() * 1.25f) * 0.5);
					set_cursor(cursor_pos);
					ImGui::PushItemWidth(m_size_x * 0.25f);
					sprintf_s(local_name_buffer_imgui_ids, sizeof(local_name_buffer_imgui_ids), "##lineposCombo %d", i);

					if (ImGui::BeginCombo(local_name_buffer_imgui_ids, line_pos_enum_to_string(action_figure_data.m_line_pos[i]).c_str()))
					{
						for (int n = 0; n < IM_ARRAYSIZE(items_line_pos); n++)
						{
							bool is_selected = action_figure_data.m_line_pos[i] == (LINE_POS_ENUM)n;
							if (ImGui::Selectable(items_line_pos[n], is_selected))
							{
								action_figure_data.m_line_pos[i] = (LINE_POS_ENUM)n;
								action_figure_data.m_line_pos[i + 1] = (LINE_POS_ENUM)n;
							}
							if (is_selected)
							{
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndCombo();
					}
				}
				else
				{
					// your pos / nothing
					action_figure_data.m_line_pos[i] = LINE_POS_ENUM::LPE_INIT;
				}

			}

		}
	}

	end_window();
}


void WINDOW_POPUP_ADD_FIGURE::update()
{
	if (!m_visible)
	{
		return;
	}

	static Instance& main_instance = Instance::inst();

	set_window_pos((unsigned int)(main_instance.m_window_size.x * 0.5f), (unsigned int)(main_instance.m_window_size.y * 0.25f));
	set_window_size((unsigned int)(main_instance.m_window_size.x * 0.5f), (unsigned int)(main_instance.m_window_size.y * 0.5f));
	begin_window();

	// used to choose if we are going to create a child figure or not 
	static FIGURE_ENUM current_figure_enum = FIGURE_ENUM::FE_UNDETERMINED;

	if (ImGui::Button(u8"Salir"))
	{
		main_instance.return_input_all_windows();
		m_visible = false;
	};

	ImGui::Text(u8"Selecciona si la figura se encuentra dentro de otra figura");
	if (ImGui::Button("Si"))
	{
		// we need to choose which figure is the parent figure
		current_figure_enum = FIGURE_ENUM::FE_COMPLEX;
	}
	if (ImGui::Button("No"))
	{
		// we skip all the parent figure selection
		current_figure_enum = FIGURE_ENUM::FE_SIMPLE;

		main_instance.scroll_main_window_looking_for_figure(nullptr);
		main_instance.set_adding_new_figure_to_main_window(true, current_figure_enum);
		main_instance.create_new_figure_instance(nullptr);
		main_instance.return_input_all_windows();
		m_visible = false;
	}

	if (current_figure_enum == FIGURE_ENUM::FE_COMPLEX)
	{
		ImGui::Text(u8"Selecciona el padre de esta figura");

		ImGui::Separator();
		ImGui::Separator();
		//list all current instances

		static bool can_accept_new_figure = false;
		// recursive function to show all the childs, as 
		// - fig 1
		// - - fig 1.1
		// - - fig 1.2
		// - - fig 1.3
		// - fig 2
		// - fig 3

		static FIGURE_ACTIONS* possible_parent = nullptr;
		FIGURE_ACTIONS* tmp_parent = nullptr;
		for (auto&& current_figure : main_instance.get_current_figures_instances().m_all_figures)
		{
			recursive_draw_child_figures(*current_figure.get(), 0, &tmp_parent);
			possible_parent = (tmp_parent != nullptr) ? tmp_parent : possible_parent;
			can_accept_new_figure = (possible_parent != nullptr) ? possible_parent : can_accept_new_figure;
			//if (ImGui::RadioButton("", true))
			//{
			//	main_instance.scroll_main_window_looking_for_figure(&current_figure);
			//	can_accept_new_figure = true;
			//}
			//ImGui::SameLine();
			//ImGui::Text(current_figure.m_actions_data.m_name.c_str());			
		}


		if (can_accept_new_figure)
		{
			if (ImGui::Button(u8"Usar esta figura como padre"))
			{
				main_instance.set_adding_new_figure_to_main_window(true, current_figure_enum);
				main_instance.create_new_figure_instance(possible_parent);
				main_instance.return_input_all_windows();
				m_visible = false;
				possible_parent = nullptr;
				can_accept_new_figure = false;
			}
		}

		// before calling this we need to figure out who is the parent of this figure

		//main_instance.set_adding_new_figure_to_main_window(true);		
	}

	end_window();
}

void WINDOW_POPUP_ADD_FIGURE::recursive_draw_child_figures(FIGURE_ACTIONS& fig, const int spaces, FIGURE_ACTIONS** to_return)
{
	static Instance& main_instance = Instance::inst();

	char buffer[256];
	sprintf_s(buffer, sizeof(char) * 256, "##%d", fig.m_id);
	if (ImGui::RadioButton(buffer, true))
	{
		main_instance.scroll_main_window_looking_for_figure(&fig);
		*to_return = &fig;
		//can_accept_new_figure = true;
	}
	sprintf_s(buffer, sizeof(char) * 256, "%s : %d", fig.m_actions_data.m_name.c_str(), fig.m_id);
	ImGui::SameLine();
	ImGui::SetCursorPosX((spaces * 15.0f) + 35.0f);
	ImGui::Text(buffer);

	for (auto&& childs : fig.m_childs)
	{
		if (childs)
		{
			recursive_draw_child_figures(*childs, spaces + 1, to_return);
		}
	}
	to_return = to_return;
}


//----------------------------------------------//

void Instance::set_audio(const char * audio_path)
{
	m_audio.load(audio_path);
	m_audio_lenght = m_audio.getLength();
}

void Instance::play_audio()
{
	audio_handle = soloud.play(m_audio);
	soloud.mGlobalVolume = 0.05f;
}

void Instance::mask_all(const int mask_to_apply)
{
	for (auto&& window : m_all_windows)
	{
		window->m_window_mask &= mask_to_apply;
	}
	resort_all_windows();
}

void Instance::or_all(const int mask_to_apply)
{
	for (auto&& window : m_all_windows)
	{
		window->m_window_mask |= mask_to_apply;
	}
	resort_all_windows();
}

void Instance::lock_all_but_one(WINDOW_LAYOUT * keep_unlocked)
{
	// lock all, but then unlock only the one we care about
	or_all(ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
	keep_unlocked->m_window_mask = keep_unlocked->m_window_mask &  ~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs;
	keep_unlocked->m_visible = true;
}

void Instance::resort_all_windows()
{
	std::sort(m_all_windows.begin(), m_all_windows.end(), [](const WINDOW_LAYOUT* a, const WINDOW_LAYOUT* b)
	{
		return a->m_draw_priority > b->m_draw_priority; //reversed, cause the last draw has higher priority
	});
}


std::shared_ptr<ACTION_FIGURE_DATA> Instance::get_data_by_name(const std::string& name)
{
	for (auto&& current_data : m_all_actions_data)
	{
		if (current_data->m_name == name)
		{
			return current_data;
		}
	}
	return std::shared_ptr<ACTION_FIGURE_DATA>();
}

void Instance::open_pop_up_window(const WINDOW_LAYOUT_TYPE window_enum)
{
	for (auto&& window : m_all_windows)
	{
		if (window->m_window_type == window_enum)
		{
			lock_all_but_one(window);
			break;
		}
	}
}

void Instance::return_input_all_windows()
{
	mask_all(~ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
}

void Instance::create_new_data_figure(ACTION_FIGURE_DATA& data)
{
	m_all_actions_data.push_back(std::make_shared<ACTION_FIGURE_DATA>(data));
}

void Instance::create_new_figure_instance(FIGURE_ACTIONS* parent)
{
	for (auto&& window : m_all_windows) // could be array using the enums as index...
	{
		if (window->m_window_type == WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT)
		{
			auto&& current_window = static_cast<WINDOW_MAIN_EDIT_TEXT*>(window);
			auto&& current_data = current_window->get_data().m_current_adding_action_data;
			if (current_data)
			{
				if (parent)
				{
					parent->m_childs.push_back(std::make_shared<FIGURE_ACTIONS>(*current_data, parent));
					current_window->get_data().m_current_edited_figure = parent->m_childs.back();
					get_current_figures_instances().figure_to_create = current_window->get_data().m_current_edited_figure;
				}
				else
				{
					get_current_figures_instances().m_all_figures.push_back(std::make_shared<FIGURE_ACTIONS>(*current_data, parent));
					current_window->get_data().m_current_edited_figure = get_current_figures_instances().m_all_figures.back();
					get_current_figures_instances().figure_to_create = current_window->get_data().m_current_edited_figure;
				}
			}
			break;
		}
	}
}

void Instance::set_edit_entry(ENTRY * entry_to_edit)
{
	for (auto&& window : m_all_windows) // could be array using the enums as index... 
	{
		if (window->m_window_type == WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_EDIT_ENTRIES)
		{
			static_cast<WINDOW_POPUP_EDIT_ENTRIES*>(window)->m_editable_entry = entry_to_edit;
			sprintf(input_text_edit, "%s", entry_to_edit->m_text.c_str());
			sprintf(input_text_notes, "%s", entry_to_edit->notes.c_str());
			break;
		}
	}
}


void Instance::set_adding_new_figure_to_main_window(const bool b, const FIGURE_ENUM& current_figure_enum)
{
	for (auto&& window : m_all_windows) // could be array using the enums as index... 
	{
		if (window->m_window_type == WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT)
		{
			static_cast<WINDOW_MAIN_EDIT_TEXT*>(window)->get_data().m_adding_new_figure = b;
			static_cast<WINDOW_MAIN_EDIT_TEXT*>(window)->get_data().m_current_figure_enum = current_figure_enum;
			break;
		}
	}
}

void Instance::scroll_main_window_looking_for_figure(FIGURE_ACTIONS* figure)
{
	for (auto&& window : m_all_windows) // could be array using the enums as index... 
	{
		if (window->m_window_type == WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT)
		{
			static_cast<WINDOW_MAIN_EDIT_TEXT*>(window)->scroll_to_this_figure(figure);
			break;
		}
	}
}

void WINDOW_EDIT_FIGURES::update()
{
	if (!m_visible)
	{
		return;
	}

	static Instance& main_instance = Instance::inst();

	begin_window();

	ImGui::Text(u8"					Edici�n de figuras");
	ImGui::Separator();
	ImGui::Text("");

	auto&& all_instanced_figures = main_instance.get_current_figures_instances().m_all_figures;
	for (auto&& fig : all_instanced_figures)
	{
		recursive_draw(fig);
	}

	end_window();
}

void WINDOW_EDIT_FIGURES::recursive_draw(std::shared_ptr<FIGURE_ACTIONS> fig, const float space)
{
	static Instance& main_instance = Instance::inst();
	if (main_instance.get_current_figures_instances().figure_to_create)
	{
		if (main_instance.get_current_figures_instances().figure_to_create->m_id == fig->m_id)
		{
			return;
		}
	}

	ImGui::Separator();
	ImGui::Text("Fig Id: %d", fig->m_id);
	ImGui::SameLine();
	ImGui::SetCursorPosX((space * 15.0f) + 100.0f);
	ImGui::Text("%s", fig->m_actions_data.m_name.c_str());
	ImGui::Text(u8"Trazos");

	int i = 0;
	for (auto&& action : fig->m_actions)
	{
		ImGui::Text((action->m_type == ACTION_ENUM::AE_INICIATIVE) ? u8"Iniciativa" : u8"Reactiva");
		sprintf_s(local_name_buffer_imgui_ids, sizeof(local_name_buffer_imgui_ids), "##linetypeCombo %d%dd", fig->m_id, i++);
		ImGui::PushItemWidth(m_size_x * 0.25f);
		if (ImGui::BeginCombo(local_name_buffer_imgui_ids, line_type_enum_to_string(action->m_line_type).c_str()))
		{
			for (int n = 0; n < IM_ARRAYSIZE(items_line_type); n++)
			{
				bool is_selected = action->m_line_type == (LINE_TYPE_ENUM)n;
				if (ImGui::Selectable(items_line_type[n], is_selected))
				{
					action->m_line_type = (LINE_TYPE_ENUM)n;
				}
				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}



		action->m_line_pos;
		action->m_line_type;
	}

	for (auto&& child : fig->m_childs)
	{
		recursive_draw(child, space + 1.0f);
	}

}

void WINDOW_POPUP_EDIT_ENTRIES::update()
{
	if (!m_visible)
	{
		return;
	}

	static Instance& main_instance = Instance::inst();

	set_window_pos((unsigned int)(main_instance.m_window_size.x * 0.25f), (unsigned int)(main_instance.m_window_size.y * 0.25f));
	set_window_size((unsigned int)(main_instance.m_window_size.x * 0.5f), (unsigned int)(main_instance.m_window_size.y * 0.5f));

	begin_window();

	if (ImGui::Button(u8"Salir"))
	{
		main_instance.return_input_all_windows();
		m_visible = false;
		m_editable_entry = nullptr;
	}
	else
	{
		ImGui::Text(u8"						Edici�n de entradas");
		ImGui::Separator();
		ImGui::Text("");

		const float init_seconds = m_editable_entry->m_time.x * (int(m_editable_entry->m_time.x) % 60);
		const float init_minutes = m_editable_entry->m_time.x * (int(m_editable_entry->m_time.x * 0.016666f) % 60);
		const float init_hours = m_editable_entry->m_time.x * (int(m_editable_entry->m_time.x * 0.000277777f) % 24);

		const float end_seconds = m_editable_entry->m_time.x * (int(m_editable_entry->m_time.y) % 60);
		const float end_minutes = m_editable_entry->m_time.x * (int(m_editable_entry->m_time.y * 0.016666f) % 60);
		const float end_hours = m_editable_entry->m_time.x * (int(m_editable_entry->m_time.y * 0.000277777f) % 24);


		ImGui::Text(u8"Tiempos de esta entrada:");
		ImGui::Separator();
		ImGui::Text(u8"Internos : %f - %f", m_editable_entry->m_time.x, m_editable_entry->m_time.y);

		ImGui::Text(u8"Horas/Minutos/Segundos : %f - %f - %f", init_hours, init_minutes, init_seconds);
		ImGui::Text(u8"Horas/Minutos/Segundos : %f - %f - %f", end_hours, end_minutes, end_seconds);

		ImGui::Separator();
		ImGui::Separator();

		ImGui::Text("");

		ImGui::Text(u8"Editar entrada - %d / 4096", strlen(input_text_edit));
		ImGui::InputTextMultiline("##1", input_text_edit, sizeof(char) * 4096);
		if (ImGui::Button("Aplicar##Edit"))
		{
			m_editable_entry->m_text = input_text_edit;
		}

		ImGui::Text(u8"A�adir anotaciones - %d / 4096", strlen(input_text_notes));
		ImGui::InputTextMultiline("##2", input_text_notes, sizeof(char) * 4096);
		if (ImGui::Button("Aplicar##Nott"))
		{
			m_editable_entry->notes = input_text_notes;
		}

	};

	end_window();
}

void WINDOW_AUDIO::update()
{
	static Instance& main_instance = Instance::inst();
	auto&& audio = main_instance.m_audio;
	auto&& core = main_instance.soloud;
	auto&& sound_id = (main_instance.audio_handle != -1) ? main_instance.audio_handle : 0;
	float current_played = core.getStreamPosition(sound_id);

	begin_window();

	ImGui::Text("--Audio Dummy-- %f", current_played);
	ImGui::Text(u8"archivo de audio actual %s", main_instance.m_audio_path.c_str());

	if (ImGui::Button("Restart"))
	{
		main_instance.play_audio();
	};
	ImGui::SameLine();
	if (ImGui::Button("Resume"))
	{
		core.setPause(main_instance.audio_handle, false);
	};
	ImGui::SameLine();
	if (ImGui::Button("Pause"))
	{
		core.setPause(main_instance.audio_handle, true);
	};
	ImGui::SameLine();
	if (ImGui::Button("Stop"))
	{
		audio.stop();
	};

	ImGui::SameLine();
	ImGui::Text("Volumen");

	ImGui::SameLine();
	float volume = core.mGlobalVolume;
	ImGui::PushItemWidth(50.0f);
	if (ImGui::DragFloat("", &volume, 0.05f, 0.0f, 1.0f))
	{
		core.mGlobalVolume = volume;
	};

	float audio_lengh = main_instance.m_audio_lenght;
	float percentage_played = current_played / audio_lengh;

	float cursor_max_time = main_instance.m_max_time;
	float cursor_max_heigh = main_instance.m_max_heigh;

	float cursor_heigh = (current_played  * cursor_max_heigh) / cursor_max_time;
	main_instance.m_audio_height_main_window = cursor_heigh;
	main_instance.m_audio_currentl_time = current_played;

	end_window();
}


//------------------------------------------//



static sf::Color default_colours[] =
{
	sf::Color(1.0f,0.0f,0.0f),
	sf::Color(0.0f,1.0f,0.0f),
	sf::Color(0.0f,0.0f,1.0f),
	sf::Color(1.0f,1.0f,0.0f),
	sf::Color(0.0f,1.0f,1.0f),
	sf::Color(1.0f,0.0f,1.0f),

	sf::Color(0.5f,1.0f,1.0f),
	sf::Color(1.0f,0.5f,1.0f),
	sf::Color(1.0f,1.0f,0.5f),

	sf::Color(0.5f,0.0f,1.0f),
	sf::Color(1.0f,0.5f,0.0f),
	sf::Color(0.0f,1.0f,0.5f),

	sf::Color(1.0f,1.0f,1.0f)
};

COLOUR_PROFILES::COLOUR_PROFILES()
{
	for (auto&& colour : default_colours)
	{
		m_colours.push_back(sf::Color(colour.r * 255.0, colour.g * 255.0, colour.b * 255.0));
	}
}




void WINDOW_MAIN_MENU::update()
{
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			static Instance& main_instance = Instance::inst();
			if (ImGui::MenuItem("Guardar", "archivos por defecto"))
			{
				main_instance.m_save = true;
				main_instance.open_pop_up_window(WINDOW_LAYOUT_TYPE::WLT_WINDOW_FILE_MANAGER);
			}
			if (ImGui::MenuItem("Cargar", "archivos por defecto"))
			{
				main_instance.m_save = false;
				main_instance.open_pop_up_window(WINDOW_LAYOUT_TYPE::WLT_WINDOW_FILE_MANAGER);
			}
			ImGui::EndMenu();
		}
		//if (ImGui::BeginMenu("Edit"))
		//{
		//	if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
		//	if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
		//	ImGui::Separator();
		//	if (ImGui::MenuItem("Cut", "CTRL+X")) {}
		//	if (ImGui::MenuItem("Copy", "CTRL+C")) {}
		//	if (ImGui::MenuItem("Paste", "CTRL+V")) {}
		//	ImGui::EndMenu();
		//}
		ImGui::EndMainMenuBar();
	}
}

void WINDOW_MAIN_MENU::save(std::string& path, FILE_MANAGER_SERIALIZE_OPTION option_selected)
{

	static Instance& main_instance = Instance::inst();

	if (path.find_last_of(".") == -1)
	{
		path += ".xml";
	}


	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_figues_data_string)
	{
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
		{
			doc.Clear();
			doc.SaveFile((path).c_str());
		}
	}
	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_figues_data_string)
	{
		tinyxml2::XMLDocument doc;

		auto&& title = doc.NewElement("FIGURES");
		doc.InsertEndChild(title);
		for (auto&& figures : main_instance.m_all_actions_data)
		{
			if (figures->m_other_figures.size() == 0)
			{
				auto&& entry_point = doc.NewElement("FIGURE");
				title->InsertEndChild(entry_point);

				auto&& xml_name = doc.NewElement("ACTION_FIGURE");
				auto&& xml_recursive = doc.NewElement("RECURSIVE");
				auto&& xml_actions = doc.NewElement("ACTIONS");
				auto&& xml_line_type = doc.NewElement("LINE_TYPE");
				auto&& xml_line_pos = doc.NewElement("LINE_POS");

				xml_name->SetAttribute("name", figures->m_name.c_str());
				xml_name->SetAttribute("recursive", figures->m_recursive);

				for (auto&& act : figures->m_actions)
				{
					auto&& action_to_store = doc.NewElement("action");
					action_to_store->SetAttribute("ACTION_ENUM", act);
					xml_actions->InsertEndChild(action_to_store);
				}
				for (auto&& act : figures->m_line_types)
				{
					auto&& action_to_store = doc.NewElement("line_type");
					action_to_store->SetAttribute("LINE_TYPE_ENUM", act);
					xml_line_type->InsertEndChild(action_to_store);
				}
				for (auto&& act : figures->m_line_pos)
				{
					auto&& action_to_store = doc.NewElement("line_pos");
					action_to_store->SetAttribute("LINE_POS_ENUM", act);
					xml_line_pos->InsertEndChild(action_to_store);
				}

				entry_point->InsertEndChild(xml_name);
				entry_point->InsertEndChild(xml_recursive);
				entry_point->InsertEndChild(xml_actions);
				entry_point->InsertEndChild(xml_line_type);
				entry_point->InsertEndChild(xml_line_pos);
			}
			else
			{
				auto&& entry_point = doc.NewElement("FIGURE_BASED_FIGURE");
				title->InsertEndChild(entry_point);

				auto&& xml_name = doc.NewElement("ACTION_FIGURE");
				xml_name->SetAttribute("name", figures->m_name.c_str());

				auto&& xml_other_figures_names = doc.NewElement("OTHER_FIGURE_NAMES");
				for (auto&& act : figures->m_other_figures)
				{
					auto&& action_to_store = doc.NewElement("OTHER_FIGURE");
					action_to_store->SetAttribute("FIGURE_NAME", act->m_name.c_str());
					xml_other_figures_names->InsertEndChild(action_to_store);
				}
				entry_point->InsertEndChild(xml_name);
				entry_point->InsertEndChild(xml_other_figures_names);
			}
		}
		doc.SaveFile((path).c_str());
	}



	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_colour_profile_string)
	{
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
		{
			doc.Clear();
			doc.SaveFile((path).c_str());
		}
	}
	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_colour_profile_string)
	{
		tinyxml2::XMLDocument doc;

		auto&& title = doc.NewElement("COLOUR_PROFILES");
		doc.InsertEndChild(title);
		int position = 0;
		for (auto&& figures : main_instance.m_colour_profile.m_colours)
		{
			auto&& col = doc.NewElement("COLOURS");
			title->InsertEndChild(col);
			col->SetAttribute("ID", position++);
			col->SetAttribute("R", figures.r);
			col->SetAttribute("G", figures.g);
			col->SetAttribute("B", figures.b);
		}
		doc.SaveFile((path).c_str());
	}

	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_entries_string)
	{
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
		{
			doc.Clear();
			doc.SaveFile((path).c_str());
		}
	}
	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_entries_string)
	{
		tinyxml2::XMLDocument doc;

		auto&& title = doc.NewElement("DATA_USED");
		doc.InsertEndChild(title);
		int position = 0;

		auto&& window = (WINDOW_MAIN_EDIT_TEXT*)main_instance.window_layout(WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT);
		for (auto&& element : window->get_data().m_all_entries)
		{
			auto&& element_doc = doc.NewElement("ENTRY");
			title->InsertEndChild(element_doc);

			auto&& element_internal_id = element.internal_id;
			auto&& element_participant = element.m_participant;
			auto&& element_participant_id = element.m_participant_id;
			auto&& element_text = element.m_text;
			auto&& element_time = element.m_time;
			auto&& element_notes = element.notes;

			auto&& element_internal_id_doc = doc.NewElement("internal_id");
			auto&& element_participant_doc = doc.NewElement("participant");
			auto&& element_participant_id_doc = doc.NewElement("participant_id");
			auto&& element_text_doc = doc.NewElement("text");
			auto&& element_time_doc = doc.NewElement("times");
			auto&& element_notes_doc = doc.NewElement("notes");

			element_doc->InsertEndChild(element_internal_id_doc);
			element_doc->InsertEndChild(element_participant_doc);
			element_doc->InsertEndChild(element_participant_id_doc);
			element_doc->InsertEndChild(element_time_doc);
			element_doc->InsertEndChild(element_text_doc);
			element_doc->InsertEndChild(element_notes_doc);

			element_internal_id_doc->SetAttribute("internal_id", element_internal_id);
			element_participant_doc->SetAttribute("participant", element_participant.c_str());
			element_participant_id_doc->SetAttribute("participant_id", element_participant_id);
			element_time_doc->SetAttribute("time_min", element_time.x);
			element_time_doc->SetAttribute("time_max", element_time.y);

			element_text_doc->SetAttribute("text_size", (int)element_text.size());
			element_notes_doc->SetAttribute("text_size", (int)element_notes.size());

			element_text_doc->SetText(element_text.c_str());
			element_notes_doc->SetText(element_notes.c_str());
		}


		doc.SaveFile((path).c_str());
	}

	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_web_solution_string)
	{
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
		{
			doc.Clear();
			doc.SaveFile((path).c_str());
		}
	}
	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_web_solution_string)
	{
		tinyxml2::XMLDocument doc;

		auto&& title = doc.NewElement("WEB_SOLUTION_DATA_FORMAT");
		doc.InsertEndChild(title);
		int position = 0;
		for (auto&& element : main_instance.get_current_figures_instances().m_all_figures)
		{
			auto&& element_doc = doc.NewElement("FIGURE");
			title->InsertEndChild(element_doc);

			// only interested about which id should be linked to 
			auto&& element_actions = element->m_actions;
			for (auto&& action : element_actions)
			{
				unsigned int element_depth = action->m_level_hierarchy;

				unsigned int element_internal_id = action->m_origin.internal_id;
				LINE_TYPE_ENUM element_line_type_first = action->m_line_type;
				LINE_POS_ENUM element_line_pos_first = action->m_line_pos;

				unsigned int element_next_id = element_internal_id;
				LINE_TYPE_ENUM element_line_type_second = element_line_type_first;
				LINE_POS_ENUM element_line_pos_second = action->m_line_pos;

				auto&& element_next = action->m_next_link;
				if (element_next)
				{
					element_next_id = element_next->m_origin.internal_id;
					element_line_type_second = element_next->m_line_type;
					element_line_pos_second = action->m_line_pos;
				}
				element_doc->SetAttribute("HIERARCHY", element_depth);

				auto&& element_init_id = doc.NewElement("INIT_ID");
				auto&& element_end_id = doc.NewElement("END_ID");

				auto&& element_init_line_type = doc.NewElement("LINE_INIT_TYPE");
				auto&& element_end_line_type = doc.NewElement("LINE_END_TYPE");

				auto&& element_init_line_pos = doc.NewElement("LINE_INIT_POS");
				auto&& element_end_line_pos = doc.NewElement("LINE_INIT_POS");

				element_doc->InsertEndChild(element_init_id);
				element_doc->InsertEndChild(element_init_line_type);
				element_doc->InsertEndChild(element_init_line_pos);

				element_doc->InsertEndChild(element_end_id);
				element_doc->InsertEndChild(element_end_line_type);
				element_doc->InsertEndChild(element_end_line_pos);

				element_init_id->SetAttribute("element_init_id", element_internal_id);
				element_init_line_type->SetAttribute("element_init_line_type", element_line_type_first);
				element_init_line_pos->SetAttribute("element_init_line_pos", element_line_pos_first);
				element_end_id->SetAttribute("element_end_id", element_next_id);
				element_end_line_type->SetAttribute("element_end_line_type", element_line_type_second);
				element_end_line_pos->SetAttribute("element_end_line_pos", element_line_pos_second);
			}

		}


		doc.SaveFile((path).c_str());
	}

	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_figures_use_string)
	{
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
		{
			doc.Clear();
			doc.SaveFile((path).c_str());
		}
	}
	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_default_file_figures_use_string)
	{
		tinyxml2::XMLDocument doc;

		auto&& title = doc.NewElement("FIGURES_USE");
		doc.InsertEndChild(title);
		int position = 0;
		for (auto&& element : main_instance.m_all_actions_data)
		{
			auto&& element_doc = doc.NewElement("FIGURE");
			title->InsertEndChild(element_doc);
			element_doc->SetAttribute("figure_name", element->m_name.c_str());
			element_doc->SetAttribute("references", element->m_references);
		}

		doc.SaveFile((path).c_str());
	}

	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_serialized_string)
	{
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
		{
			doc.Clear();
			doc.SaveFile((path).c_str());
		}
	}
	if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_serialized_string)
	{
		tinyxml2::XMLDocument doc;

		auto&& title = doc.NewElement("SERIALIZED_DATA_TEXT");
		doc.InsertEndChild(title);

		auto&& entry_parent_node = doc.NewElement("ALL_ENTRIES");
		// output all the text as usual
		title->InsertEndChild(entry_parent_node);

		auto&& window = (WINDOW_MAIN_EDIT_TEXT*)main_instance.window_layout(WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT);
		for (auto&& element : window->get_data().m_all_entries)
		{
			auto&& element_doc = doc.NewElement("ENTRY");
			entry_parent_node->InsertEndChild(element_doc);

			auto&& element_internal_id = element.internal_id;
			auto&& element_participant = element.m_participant;
			auto&& element_participant_id = element.m_participant_id;
			auto&& element_text = element.m_text;
			auto&& element_time = element.m_time;
			auto&& element_notes = element.notes;

			auto&& element_internal_id_doc = doc.NewElement("internal_id");
			auto&& element_participant_doc = doc.NewElement("participant");
			auto&& element_participant_id_doc = doc.NewElement("participant_id");
			auto&& element_text_doc = doc.NewElement("text");
			auto&& element_time_doc = doc.NewElement("times");
			auto&& element_notes_doc = doc.NewElement("notes");

			element_doc->InsertEndChild(element_internal_id_doc);
			element_doc->InsertEndChild(element_participant_doc);
			element_doc->InsertEndChild(element_participant_id_doc);
			element_doc->InsertEndChild(element_time_doc);
			element_doc->InsertEndChild(element_text_doc);
			element_doc->InsertEndChild(element_notes_doc);

			element_internal_id_doc->SetAttribute("internal_id", element_internal_id);
			element_participant_doc->SetAttribute("participant", element_participant.c_str());
			element_participant_id_doc->SetAttribute("participant_id", element_participant_id);
			element_time_doc->SetAttribute("time_min", element_time.x);
			element_time_doc->SetAttribute("time_max", element_time.y);

			element_text_doc->SetAttribute("text_size", (int)element_text.size());
			element_notes_doc->SetAttribute("text_size", (int)element_notes.size());

			element_text_doc->SetText(element_text.c_str());
			element_notes_doc->SetText(element_notes.c_str());
		}
		// output all the figures
		auto&& title2 = doc.NewElement("SERIALIZED_DATA_FIGURES");
		for (auto&& figures : main_instance.m_all_actions_data)
		{
			if (figures->m_other_figures.size() == 0)
			{
				auto&& entry_point = doc.NewElement("FIGURE");
				title2->InsertEndChild(entry_point);

				auto&& xml_name = doc.NewElement("ACTION_FIGURE");
				auto&& xml_recursive = doc.NewElement("RECURSIVE");
				auto&& xml_actions = doc.NewElement("ACTIONS");
				auto&& xml_line_type = doc.NewElement("LINE_TYPE");
				auto&& xml_line_pos = doc.NewElement("LINE_POS");

				xml_name->SetAttribute("name", figures->m_name.c_str());
				xml_name->SetAttribute("recursive", figures->m_recursive);
				xml_name->SetAttribute("references", figures->m_references);

				for (auto&& act : figures->m_actions)
				{
					auto&& action_to_store = doc.NewElement("action");
					action_to_store->SetAttribute("ACTION_ENUM", act);
					xml_actions->InsertEndChild(action_to_store);
				}
				for (auto&& act : figures->m_line_types)
				{
					auto&& action_to_store = doc.NewElement("line_type");
					action_to_store->SetAttribute("LINE_TYPE_ENUM", act);
					xml_line_type->InsertEndChild(action_to_store);
				}
				for (auto&& act : figures->m_line_pos)
				{
					auto&& action_to_store = doc.NewElement("line_pos");
					action_to_store->SetAttribute("LINE_POS_ENUM", act);
					xml_line_pos->InsertEndChild(action_to_store);
				}

				entry_point->InsertEndChild(xml_name);
				entry_point->InsertEndChild(xml_recursive);
				entry_point->InsertEndChild(xml_actions);
				entry_point->InsertEndChild(xml_line_type);
				entry_point->InsertEndChild(xml_line_pos);
			}
			else
			{
				auto&& entry_point = doc.NewElement("FIGURE_BASED_FIGURE");
				title2->InsertEndChild(entry_point);

				auto&& xml_name = doc.NewElement("ACTION_FIGURE");
				xml_name->SetAttribute("name", figures->m_name.c_str());
				xml_name->SetAttribute("references", figures->m_references);

				auto&& xml_other_figures_names = doc.NewElement("OTHER_FIGURE_NAMES");
				for (auto&& act : figures->m_other_figures)
				{
					auto&& action_to_store = doc.NewElement("OTHER_FIGURE");
					action_to_store->SetAttribute("FIGURE_NAME", act->m_name.c_str());
					xml_other_figures_names->InsertEndChild(action_to_store);
				}
				entry_point->InsertEndChild(xml_name);
				entry_point->InsertEndChild(xml_other_figures_names);
			}
		}

		auto&& title3 = doc.NewElement("SERIALIZED_DATA_ACTIONS");
		for (auto&& element : main_instance.get_current_figures_instances().m_all_figures)
		{
			auto&& element_doc = doc.NewElement("FIGURE");
			title3->InsertEndChild(element_doc);
			element_doc->SetAttribute("Figure_name", element->m_actions_data.m_name.c_str());
			element_doc->SetAttribute("Figure_id", element->m_id);
			element_doc->SetAttribute("Parent_id", (element->m_parent) ? element->m_parent->m_id : -1);
			element->save(doc, *element_doc);
		}

		title->InsertEndChild(title2);
		title->InsertEndChild(title3);

		doc.SaveFile((path).c_str());
	}

}

void WINDOW_MAIN_MENU::load(const std::string& path, FILE_MANAGER_SERIALIZE_OPTION option_selected)
{
	static Instance& main_instance = Instance::inst();
	auto&& current_window = static_cast<WINDOW_MAIN_EDIT_TEXT*>(main_instance.window_layout(WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT));

	// load eaf or internal file
	auto&& loc_of_dot = path.find_last_of(".");
	std::string format = path.substr((loc_of_dot == -1) ? 0 : loc_of_dot, path.size());
	if (format.compare(".eaf") == 0)
	{
		((WINDOW_MAIN_EDIT_TEXT*)(main_instance.window_layout(WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT)))->set_entries(parse_file(path));
		main_instance.get_current_figures_instances().m_all_figures.clear();
	}
	else
	{
		if (option_selected == FILE_MANAGER_SERIALIZE_OPTION::FMSO_serialized_string)
		{
			tinyxml2::XMLDocument doc;
			if (!doc.LoadFile((path).c_str()) == tinyxml2::XML_SUCCESS)
			{
				// assert, does not exist...
				return;
			}
			else
			{
				auto&& main_window = ((WINDOW_MAIN_EDIT_TEXT*)(main_instance.window_layout(WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT)));
				// get all entries data
				auto&& root = doc.FirstChildElement("SERIALIZED_DATA_TEXT");
				if (root)
				{
					// get entries on here
					auto&& main_entries_root = root->FirstChildElement("ALL_ENTRIES");
					if (main_entries_root)
					{
						auto&& single_entry_node = main_entries_root->FirstChildElement("ENTRY");
						if (single_entry_node)
						{
							main_window->set_entries(std::vector<ENTRY>());
						}
						else
						{
							return;
						}
						while (single_entry_node)
						{
							// get all the data on here
							{
								auto&& internal_id = single_entry_node->FirstChildElement("internal_id");
								auto&& participant = single_entry_node->FirstChildElement("participant");
								auto&& participant_id = single_entry_node->FirstChildElement("participant_id");
								auto&& times = single_entry_node->FirstChildElement("times");
								auto&& text = single_entry_node->FirstChildElement("text");
								auto&& notes = single_entry_node->FirstChildElement("notes");

								auto&& internal_id_value = internal_id->IntAttribute("internal_id");
								auto&& participant_value = participant->Attribute("participant");
								auto&& participant_id_value = participant_id->IntAttribute("participant_id");
								auto&& times_min_value = times->FloatAttribute("time_min");
								auto&& times_max_value = times->FloatAttribute("time_max");
								auto&& text_value = text->GetText();
								auto&& notes_value = notes->GetText();

								text_value = (text_value) ? text_value : "";
								notes_value = (notes_value) ? notes_value : "";


								ENTRY entry;
								entry.internal_id = internal_id_value;
								entry.m_participant = participant_value;
								entry.m_participant_id = participant_id_value;
								entry.m_time.x = times_min_value;
								entry.m_time.y = times_max_value;
								entry.m_text = text_value;
								entry.notes = notes_value;

								main_window->add_entry(entry);
							}
							single_entry_node = single_entry_node->NextSiblingElement();
						}
					}
					//now get the data of each figure on this file
					auto&& figures_data_root = root->FirstChildElement("SERIALIZED_DATA_FIGURES");
					main_instance.m_all_actions_data.clear();

					if (figures_data_root)
					{
						auto&& figures = figures_data_root->FirstChildElement("FIGURE");
						while (figures)
						{
							std::shared_ptr<ACTION_FIGURE_DATA> data_figure = std::make_shared<ACTION_FIGURE_DATA>();
							{
								auto&& name_recursive = figures->FirstChildElement("ACTION_FIGURE");
								auto&& all_actions = figures->FirstChildElement("ACTIONS");;
								auto&& all_line_types = figures->FirstChildElement("LINE_TYPE");;
								auto&& all_line_pos = figures->FirstChildElement("LINE_POS");;

								std::vector<ACTION_ENUM>	all_actions_values;
								std::vector<LINE_TYPE_ENUM> all_line_type_values;
								std::vector<LINE_POS_ENUM>	all_line_pos_values;

								for (auto&& element = all_actions->FirstChild(); element; element = element->NextSiblingElement())
								{
									all_actions_values.push_back((ACTION_ENUM)element->ToElement()->IntAttribute("ACTION_ENUM"));
								}
								for (auto&& element = all_line_types->FirstChild(); element; element = element->NextSiblingElement())
								{
									all_line_type_values.push_back((LINE_TYPE_ENUM)element->ToElement()->IntAttribute("LINE_TYPE_ENUM"));
								}
								for (auto&& element = all_line_pos->FirstChild(); element; element = element->NextSiblingElement())
								{
									all_line_pos_values.push_back((LINE_POS_ENUM)element->ToElement()->IntAttribute("LINE_POS_ENUM"));
								}

								data_figure->m_actions = all_actions_values;
								data_figure->m_line_pos = all_line_pos_values;
								data_figure->m_line_types = all_line_type_values;
								data_figure->m_name = name_recursive->Attribute("name");
								data_figure->m_recursive = name_recursive->BoolAttribute("recursive");
								data_figure->m_references = name_recursive->IntAttribute("references");;
							}

							main_instance.m_all_actions_data.push_back(data_figure);

							figures = figures->NextSiblingElement("FIGURE");
						}

						auto&& figures_of_figures = figures_data_root->FirstChildElement("FIGURE_BASED_FIGURE");
						while (figures_of_figures)
						{
							std::shared_ptr<ACTION_FIGURE_DATA> data_figure = std::make_shared<ACTION_FIGURE_DATA>();

							auto&& name_recursive = figures_of_figures->FirstChildElement("ACTION_FIGURE");
							data_figure->m_name = name_recursive->Attribute("name");
							data_figure->m_references = name_recursive->IntAttribute("references");;

							auto&& other_figure_names = figures_of_figures->FirstChildElement("OTHER_FIGURE_NAMES");;
							for (auto&& element = other_figure_names->FirstChild(); element; element = element->NextSiblingElement())
							{
								auto&& other_figure_name = element->ToElement()->Attribute("FIGURE_NAME");

								for (auto&& local_figure : main_instance.m_all_actions_data)
								{
									if (local_figure->m_name == other_figure_name)
									{
										data_figure->m_other_figures.push_back(local_figure);
									}
								}
							}

							main_instance.m_all_actions_data.push_back(data_figure);

							figures_of_figures = figures_of_figures->NextSiblingElement("FIGURE_BASED_FIGURE");
						}
					}

					main_instance.get_current_figures_instances().m_all_figures.clear();

					auto&& all_stored_actions = root->FirstChildElement("SERIALIZED_DATA_ACTIONS");
					if (all_stored_actions)
					{
						auto&& current_figure = all_stored_actions->FirstChildElement("FIGURE");
						while (current_figure)
						{
							//recursive_function(current_figure,main_window,main_instance);
							//auto&& possible_child = current_figure->FirstChildElement("childs");
							recursive_child(current_figure, main_window, main_instance);													
							current_figure = current_figure->NextSiblingElement();
						}
					}

				}
			}

			main_instance.get_current_figures_instances().hierarchy_check();
		}

	}
}

void recursive_function (const tinyxml2::XMLElement* entry_point, WINDOW_MAIN_EDIT_TEXT* main_window, Instance& main_instance)
{
	//auto&& current_figure = entry_point->FirstChildElement("FIGURE");
	//while (current_figure)
	{
		auto&& name_of = entry_point->Attribute("Figure_name");
		auto&& figure_id = entry_point->IntAttribute("Figure_id");
		auto&& parent_id = entry_point->IntAttribute("Parent_id");

		auto&& action = entry_point->FirstChildElement("action");
		std::vector<ACTION*> all_actions;
		while (action)
		{
			auto&& line_type_1 = action->FirstChildElement("line_type");
			auto&& line_pos_1 = action->FirstChildElement("line_pos");
			auto&& type_1 = action->FirstChildElement("type");
			auto&& line_origin_1 = action->FirstChildElement("origin");

			ACTION* new_action = nullptr;

			auto&& type_value = type_1->IntAttribute("type");
			auto&& line_type_value = line_type_1->IntAttribute("line_type");
			auto&& line_pos_value = line_pos_1->IntAttribute("line_pos");
			auto&& line_origin_value = line_origin_1->IntAttribute("origin");

			// find the entry that references this action
			auto&& current_window
				= static_cast<WINDOW_MAIN_EDIT_TEXT*>
				(main_instance.window_layout(WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT));

			auto&& referenced_entry = main_window->get_entry_by_id(line_origin_value);

			if (type_value == ACTION_ENUM::AE_INICIATIVE)
			{
				new_action = new OUT_ACTION(referenced_entry);
			}
			else if (type_value == ACTION_ENUM::AE_REACTIVE)
			{
				new_action = new IN_ACTION(referenced_entry);
			}

			new_action->m_line_pos = (LINE_POS_ENUM)line_pos_value;
			new_action->m_line_type = (LINE_TYPE_ENUM)line_type_value;
			new_action->m_type = (ACTION_ENUM)type_value;

			all_actions.push_back(new_action);

			action = action->NextSiblingElement("action");
		}

		auto&& data_of_this_action = main_instance.get_data_by_name(name_of);

		std::shared_ptr<FIGURE_ACTIONS> fig = 
			std::make_shared<FIGURE_ACTIONS>(*data_of_this_action.get(), nullptr);

		for (auto&& all_this_actions : all_actions)
		{
			fig->add_new_action(all_this_actions);
		}

		fig->m_id = figure_id;
		fig->m_parent_id = parent_id;

		main_instance.get_current_figures_instances().m_all_figures.push_back(fig);

		//current_figure = current_figure->NextSiblingElement();
	}
};

void recursive_child(tinyxml2::XMLElement* entry_point, WINDOW_MAIN_EDIT_TEXT* main_window, Instance& main_instance)
{
	if (entry_point)
	{
		recursive_function(entry_point, main_window, main_instance);		
		
		recursive_child(entry_point->FirstChildElement("childs"), main_window, main_instance);
		recursive_child(entry_point->NextSiblingElement("childs"), main_window, main_instance);
	}
};

void WINDOW_FILE_MANAGER::update()
{
	if (!m_visible)
	{
		return;
	}
	static Instance& main_instance = Instance::inst();


	int x = (int)main_instance.m_window_size.x;
	int y = (int)main_instance.m_window_size.y;

	set_window_size(x * 0.5f, y * 0.5f);
	set_window_pos(x * 0.25f, y * 0.25f);

	begin_window();

	ImGui::Text(u8"									FILE MANAGER");
	ImGui::Separator();

	static FILE_MANAGER_SERIALIZE_OPTION option_selected = FILE_MANAGER_SERIALIZE_OPTION::FMSO_unselected;

	if (ImGui::Button("Salir"))
	{
		option_selected = FILE_MANAGER_SERIALIZE_OPTION::FMSO_unselected;
		main_instance.return_input_all_windows();
		m_visible = false;
	}

	ImGui::Separator();
	ImGui::Text("");

	for (int i = 0; i < FILE_MANAGER_SERIALIZE_OPTION::FMSO_unselected; ++i)
	{
		if (ImGui::Selectable(WINDOW_MAIN_MENU::m_default_option_selected[i].c_str()))
		{
			option_selected = FILE_MANAGER_SERIALIZE_OPTION(i);
		}
	}

	ImGui::Text("");
	ImGui::Separator();
	ImGui::Separator();
	ImGui::Text("");

	//////////////////////////////////////////////////////////////////
	if (option_selected != FILE_MANAGER_SERIALIZE_OPTION::FMSO_unselected)
	{
		static std::string wholeRelativePath_ = "../../";
		static std::string wholeRelativePathToFile = "";
		char what2[250];
		ImGuiFs::PathGetAbsolute(wholeRelativePath_.c_str(), what2);
		wholeRelativePath_ = what2;

		ImGuiFs::PathStringVector directories_;
		ImGuiFs::FilenameStringVector stringVector;

		static char filename[255]{};

		if (ImGui::Button("Directiorio previo"))
		{
			if (wholeRelativePath_.back() == '/')
			{
				wholeRelativePath_ += "..";
			}
			else
			{
				wholeRelativePath_ += "/..";
			}
			char what[250];
			ImGuiFs::PathGetAbsolute(wholeRelativePath_.c_str(), what);
			wholeRelativePath_ = what;
		};
		ImGui::Text("Current: %s", wholeRelativePath_.c_str());
		ImGuiFs::DirectoryGetDirectories(wholeRelativePath_.c_str(), directories_);
		ImGui::Text("Directories");
		ImGui::Separator();

		for (auto& t : directories_)
		{
			ImGui::Text("%s", t);
			if (ImGui::IsItemClicked())
			{
				wholeRelativePath_ = t;
			}
		}

		ImGui::Separator();
		ImGui::Text("Files");
		ImGui::Separator();
		ImGuiFs::DirectoryGetFiles(wholeRelativePath_.c_str(), directories_);
		for (auto& t : directories_)
		{
			ImGui::Text("%s", t);
			if (ImGui::IsItemClicked()) {
				std::string trampa = t;
				int lastSlash = trampa.find_last_of('/');
				trampa.erase(0, lastSlash);
				sprintf(filename, "%s", trampa.c_str());
			}
		}

		ImGui::Separator();
		ImGui::InputText("Filename", filename, sizeof(filename));
		ImGui::Separator();


		char tmpText[20];
		if (main_instance.m_save)
		{
			sprintf(tmpText, "Guardar");
		}
		else
		{
			sprintf(tmpText, "Cargar");
		}

		if (ImGui::Button(tmpText))
		{
			wholeRelativePathToFile.clear();
			int last = wholeRelativePath_.find_last_of('/');
			if (last != -1) {
				if (wholeRelativePath_.back() == '/')
				{
					wholeRelativePathToFile = wholeRelativePath_ + filename;
				}
				else
				{
					wholeRelativePathToFile = wholeRelativePath_ + '/' + filename;
				}
			}
			if (main_instance.m_save)
			{
				WINDOW_MAIN_MENU::save(wholeRelativePathToFile, option_selected);
			}
			else
			{
				WINDOW_MAIN_MENU::load(wholeRelativePathToFile, option_selected);
			}

		}
	}

	end_window();

}

using namespace tinyxml2;
std::vector<ENTRY> parse_file(std::string path2)
{

	std::map<std::string, float> all_timers;	// store all the timers per each entry on the xml file
	std::vector<ENTRY> all_entries;
	const char* path = path2.c_str();
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path);

	const float timerInverse = 1.0f / 3600.0f; // used to convert to seconds

	XMLNode* annotation = doc.FirstChildElement("ANNOTATION_DOCUMENT");
	XMLNode* timers_order_parent = annotation->FirstChildElement("TIME_ORDER");
	XMLNode* timers_order_child = timers_order_parent->FirstChildElement();

	for (XMLNode* currentNode = timers_order_child; currentNode;) {
		all_timers[currentNode->ToElement()->Attribute("TIME_SLOT_ID")] = (float)atof(currentNode->ToElement()->Attribute("TIME_VALUE"));
		currentNode = currentNode->NextSiblingElement();
	}
	// loop over all the possible entries
	XMLNode* entries = annotation->FirstChildElement("TIER");
	std::string current_participant;

	while (entries) {
		const char* attribureField = entries->ToElement()->Attribute("LINGUISTIC_TYPE_REF");
		if (attribureField != NULL) {
			if (strcmp("default-lt", attribureField) == 0) {
				current_participant = entries->ToElement()->Attribute("PARTICIPANT");
				XMLNode* anotation = entries->FirstChildElement();
				while (anotation) {
					XMLNode* alignableAnnotation = anotation->FirstChildElement();
					ENTRY entry;
					entry.m_time.x = all_timers[alignableAnnotation->ToElement()->Attribute("TIME_SLOT_REF1")] * timerInverse;
					entry.m_time.y = all_timers[alignableAnnotation->ToElement()->Attribute("TIME_SLOT_REF2")] * timerInverse;
					entry.m_participant = current_participant;

					char letter = current_participant.c_str()[0];
					if (letter > 96) { letter -= 32; }
					letter -= 17;
					entry.m_participant_id = atoi(&letter);

					auto&& first_child = alignableAnnotation->FirstChildElement("ANNOTATION_VALUE");
					if (first_child->GetText() != 0)
					{
						entry.m_text = first_child->GetText();
					}
					anotation = anotation->NextSiblingElement();
					all_entries.push_back(entry);
				}
			}
		}
		entries = entries->NextSiblingElement();
	}

	std::sort(all_entries.begin(), all_entries.end(), [&](const ENTRY& a, const ENTRY& b) {
		return a.m_time.x < b.m_time.x;
	});

	unsigned int c = 0;
	for (auto& r : all_entries) {
		r.internal_id = c++;
	}
	return all_entries;
}

void FIGURES_INSTANCE::hierarchy_check()
{
	std::vector<int> to_erase_ids;

	for(auto&& rev_itr = m_all_figures.rbegin(); rev_itr != m_all_figures.rend(); ++rev_itr)
	//for (auto&& element : m_all_figures)
	{
		auto&& element = *rev_itr;
		if (element->m_parent_id != -1)
		{			
			for (auto&& possible_parent : m_all_figures) 
			{
				if (possible_parent->m_id == element->m_parent_id) 
				{
					possible_parent->m_childs.push_back(element);
					element->m_parent = possible_parent.get();
					break;
				}
			}						
			to_erase_ids.push_back(element->m_id);
		}
	}

	auto&& new_erase_end = std::remove_if(m_all_figures.begin(), m_all_figures.end(),
		[&to_erase_ids](std::shared_ptr<FIGURE_ACTIONS> action)
	{
		for (auto&& erase_id : to_erase_ids)
		{
			if (erase_id == action->m_id) { return true; }
		}
		return false;
	}
	);

	m_all_figures.erase(new_erase_end,m_all_figures.end());

}
