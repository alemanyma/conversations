﻿
@REM Compilación y Enlace con librería gráfica.
@cls
del /Q ..\tmp\*.* 
del *.pdb
mkdir ..\tmp
mkdir ..\bin
copy ..\extern\SFML\bin\*.dll ..\bin

call "%VS140COMNTOOLS%\..\..\VC\vcvarsall.bat"

cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../extern/imgui/imgui-SFML.cpp -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../extern/imgui/imgui.cpp -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../extern/imgui/imgui_draw.cpp -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../extern/imgui/imgui_demo.cpp -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/sprite.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/text.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/object.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/main.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/imgui_code.cc  -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/main_code.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/manager.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/box.cc -I ../extern/SFML/include -I .
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/circle.cc -I ../extern/SFML/include -I .                           
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/line.cc -I ../extern/SFML/include -I .                           
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/pointer_functions.cc -I ../extern/SFML/include -I .		
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/action.cc -I ../extern/SFML/include -I .		
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/action_manager.cc -I ../extern/SFML/include -I .		
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/movement.cc -I ../extern/SFML/include -I .		
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/scene.cc -I ../extern/SFML/include -I .		
cl /nologo /Zi /GR- /EHs /MDd /c /Fo.\..\tmp\ ../src/polygon.cc -I ../extern/SFML/include -I .		
											 
cl /nologo /Zi /GR- /EHs /MDd /Fe:../bin/tweening.exe ..\tmp\polygon.obj ..\tmp\scene.obj ..\tmp\movement.obj ..\tmp\action_manager.obj ..\tmp\action.obj ..\tmp\line.obj ..\tmp\pointer_functions.obj ..\tmp\circle.obj ..\tmp\box.obj ..\tmp\manager.obj ..\tmp\imgui_code.obj ..\tmp\imgui.obj ..\tmp\imgui_draw.obj ..\tmp\imgui-SFML.obj ..\tmp\imgui_demo.obj ..\tmp\text.obj ..\tmp\sprite.obj ..\tmp\main_code.obj ..\tmp\object.obj ..\tmp\main.obj  ../extern/SFML/lib/vorbis.lib ../extern/SFML/lib/vorbisenc.lib ../extern/SFML/lib/vorbisfile.lib ../extern/SFML/lib/jpeg.lib ../extern/SFML/lib/ogg.lib ../extern/SFML/lib/flac.lib ../extern/SFML/lib/freetype.lib ../extern/SFML/lib/sfml-graphics-d.lib ../extern/SFML/lib/sfml-main-d.lib  ../extern/SFML/lib/openal32.lib ../extern/SFML/lib/sfml-audio-d.lib  ../extern/SFML/lib/sfml-window-d.lib ../extern/SFML/lib/sfml-system-d.lib ../extern/SFML/lib/sfml-network-d.lib opengl32.lib user32.lib gdi32.lib shell32.lib


@echo.
@echo.
@echo    ----------------------------------------------------------------------
@echo  --------------------------------------------------------------------------
@echo ---------------------                                  ---------------------
@echo ---------------------   Proceso por lotes finalizado   ---------------------
@echo ---------------------                                  ---------------------
@echo  --------------------------------------------------------------------------
@echo    ----------------------------------------------------------------------
@echo.
@echo.
pause