
os.execute("mkdir ../bin")

solution  "Analyzer"

configuration "vs2017"
    windowstargetplatformversion "10.0.10586.0"

  kind "ConsoleApp"
 
  location "../../User/bin"
  targetdir   "../../User/bin"
  configurations  {
      "Debug",
      "Release",
  }      
  platforms {
      "x32",
      "x64",
      "Native",
  }
  
  location "../../User/bin"
  language "C++"
  
    configuration "Debug"
		flags {"Symbols"}
  
  project "Analyzer"
  
  objdir "./obj/"
  kind "ConsoleApp"
  location "../../User/bin"
  debugdir "../../User/bin"
  
  implibdir "../../extern/SFML/lib/"
  
  links {"winmm","opengl32","GLU32","../../extern/SFML/lib/vorbis",
  "../../extern/SFML/lib/vorbisenc","../../extern/SFML/lib/vorbisfile","../../extern/SFML/lib/ogg","../../extern/SFML/lib/flac","../../extern/SFML/lib/freetype",
  
   "../../extern/SFML/lib/sfml-graphics-s",
   "../../extern/SFML/lib/sfml-main",
   "../../extern/SFML/lib/openal32",
   "../../extern/SFML/lib/sfml-audio-s",
   "../../extern/SFML/lib/sfml-window-s",
   "../../extern/SFML/lib/sfml-system-s",
   "../../extern/SFML/lib/sfml-network-s",
   "../../SoLoud/build/vs2017/x64/Release/soloud_static", 

  -- "../../SoLoud/build/vs2017/x64/Debug/soloud_static", 
  -- "../../extern/SFML/lib/sfml-graphics-s-d",
  -- "../../extern/SFML/lib/sfml-main-d",
  -- "../../extern/SFML/lib/openal32",
  -- "../../extern/SFML/lib/sfml-audio-s-d",
  -- "../../extern/SFML/lib/sfml-window-s-d",
  -- "../../extern/SFML/lib/sfml-system-s-d",  
  --  "../../extern/SFML/lib/sfml-network-s-d",  

  
  "opengl32","user32","gdi32","shell32"}
  
  includedirs {
    "../../extern/SFML/include",
    "../../SoLoud/include",
    "../../extern/imguifilesystem/",
    "../../extern/imgui",
    "../../extern/tinyXML",
    "../../extern/",
    "../../src",    
  }
  
  
  defines { 
"SFML_STATIC"
		  }
  files {
  "../../src/**.cpp",
  "../../src/**.cc",
  "../../include/**.h",
  "../../src/**.c",
  "../../extern/imgui/imgui-SFML.cpp",
  "../../extern/tinyXML/*.cpp",
  
  "../../extern/imguifilesystem/imguifilesystem.cpp",  
  --"../../extern/imguifilesystem/dirent_portable.h",
  
  "../../extern/imgui/imgui.cpp",
  "../../extern/imgui/imgui_draw.cpp",
  "../../extern/imgui/imgui_demo.cpp",  
  "../../SoLoud/include/*.h",

  } 
  targetdir   "../../User/bin"
  configuration{}

    
  
  
  