#ifndef __FIGURES_H__
#define __FIGURES_H__ 1


#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include <vector>
#include <imgui.h>
#include <stdexcept>
#include <tinyxml2.h>

using namespace sf;

// the person who said something and when it is said
struct ENTRY {	
	Vector2f m_time;
	Vector2f m_text_global_pos;	
	std::string m_participant; 
	std::string m_text;// the actual string containing the text said by this person
	std::string notes; // aiming to add notes per each entry via Text area
	int m_participant_id; // id of each participant, A == 0, b == 1  and so on...	
						  //----------------------------//
	unsigned int internal_id = 0; // unique id value, to identify each entry, incremented when created
};


// class to hold all the information about a line
struct LINE 
{
	sf::Color m_color;
};

enum LINE_TYPE_ENUM
{
	LTE_CONSTANT,
	LTE_NON_CONSTANT,
	LTE_BORDER,
	LTE_COUNT,
};

enum LINE_POS_ENUM
{
	LPE_INIT,
	LPE_REACTIVE,
	LPE_MIDDPOINT,
	LPE_COUNT,
};

enum ACTION_ENUM 
{
	AE_INICIATIVE,
	AE_REACTIVE,
	// can be add, link to another figure...
	AE_COUNT,
};

enum FIGURE_ENUM
{
	FE_UNDETERMINED,
	FE_SIMPLE,	// highest level, simple figure, no childs
	FE_COMPLEX,	// being added as a child	
	FE_COUNT,
};

// Conversion functions, used mainly for serialize
const std::string& action_enum_to_string(const ACTION_ENUM action);
const ACTION_ENUM action_string_to_enum(const std::string& action);

const std::string& line_type_enum_to_string(const LINE_TYPE_ENUM action);
const LINE_TYPE_ENUM line_type_string_to_enum(const std::string& action);

const std::string& line_pos_enum_to_string(const LINE_POS_ENUM action);
const LINE_POS_ENUM line_pos_string_to_enum(const std::string& action);



// Data side of each figure representation
struct ACTION_FIGURE_DATA
{
	std::string					m_name;
	bool						m_recursive = false;
	std::vector<ACTION_ENUM>	m_actions;	
	std::vector<LINE_TYPE_ENUM> m_line_types;
	std::vector<LINE_POS_ENUM>	m_line_pos;
	
	int m_references = 0;
	
	std::vector<std::shared_ptr<ACTION_FIGURE_DATA>> m_other_figures;

	ACTION_FIGURE_DATA() 
	{

	};

	ACTION_FIGURE_DATA(const ACTION_FIGURE_DATA& other) 
		: m_name(other.m_name)
		, m_actions(other.m_actions)
		, m_line_types(other.m_line_types)
		, m_line_pos(other.m_line_pos)
		, m_recursive(other.m_recursive)
		, m_other_figures(other.m_other_figures)
		, m_references(other.m_references)
	{
	};

	// since is used as static on the creation step, needs to clean all the 
	// relevant information
	void clean() 
	{
		m_name = "";
		m_actions.clear();
		m_line_pos.clear();
		m_line_types.clear();		
		m_recursive = false;
		m_other_figures.clear();
		m_references = 0;
	}
	void pass_figure_data_to_main_window();	
};

// struct that will hold each action, individually
class ACTION
{
public:
	ACTION(const ENTRY& origin) : m_origin(origin)
	{}
	//virtual void draw() = 0;
	const ENTRY& m_origin;
	ACTION_ENUM m_type;
	virtual void draw(const int level) = 0;
	virtual void add_link(ACTION* link) = 0;
	virtual ACTION* get_link() = 0;
	ACTION* m_next_link = nullptr;
	ACTION* m_previous_link = nullptr;
	class FIGURE_ACTIONS* m_parent = nullptr;
	sf::Vector2f m_intersection_point;
	
	float m_max_height = 0.0f;
	float m_min_height = 0.0f;

	std::vector<sf::Vector2f> non_continous_line(const sf::Vector2f& init, const sf::Vector2f& end,const float divisions);
	void draw_line(const sf::Vector2f& init, const sf::Vector2f& end, int id);

	void set_min_max_height(const sf::Vector2f& init, const sf::Vector2f& end);
	// TODO: missing line config
	LINE_TYPE_ENUM m_line_type;
	LINE_POS_ENUM m_line_pos;
	
	int m_level_hierarchy = 0;
	
	void save(tinyxml2::XMLDocument& doc,tinyxml2::XMLElement& element_doc);
	void load(tinyxml2::XMLDocument& doc,tinyxml2::XMLElement& element_doc);

};
// actions that are meant to link TO any entry
class IN_ACTION : public ACTION
{	
public:
	IN_ACTION(const ENTRY& origin) 
		: ACTION(origin)
	{
		m_type = ACTION_ENUM::AE_REACTIVE;
	}
	virtual void draw(const int level) override;
	virtual void add_link(ACTION* link) override { m_previous_link = link; };
	virtual ACTION* get_link() override { return m_previous_link; };
};
// actions that are meant to link FROM any entry
class OUT_ACTION : public ACTION
{
public:
	OUT_ACTION(const ENTRY& origin) 
		: ACTION(origin)
	{
		m_type = ACTION_ENUM::AE_INICIATIVE;
	}	
	virtual void draw(const int level) override;
	virtual void add_link(ACTION* link) override { m_next_link = link; };
	virtual ACTION* get_link() override { return m_next_link; };
};
// actions that are meant to link FROM any entry, to any other action
class INTERLINK_ACTION : public ACTION
{
public:
	INTERLINK_ACTION(const ENTRY& origin, ACTION& to_intersect) 
		: ACTION(origin)
		, m_to_intersect(to_intersect)
	{
	}
	ACTION& m_to_intersect;
	virtual void draw(const int level) override {};
	virtual void add_link(ACTION* link) override { link; };
	virtual ACTION* get_link() override { return nullptr; };
};

// struct that will hold all the actions that this figure needs to be shown
static int counter_id = 0;
class FIGURE_ACTIONS
{
public:
	FIGURE_ACTIONS(ACTION_FIGURE_DATA& action_data, FIGURE_ACTIONS* parent)
		: m_actions_data(action_data)		
		, m_parent(parent)
		, m_id(counter_id++)
	{
	}
	~FIGURE_ACTIONS()		
	{
		printf("liada...");
	}
	// "parent", const reference to the data used on this figure
	ACTION_FIGURE_DATA& m_actions_data;
	// mean to loop over all actions and draw them
	void add_new_action(ACTION* new_action);
	//void draw_figure();	
	FIGURE_ACTIONS* m_parent = nullptr;
	std::vector<std::shared_ptr<FIGURE_ACTIONS>> m_childs;
	void draw(const int level);

	float m_max_height = 0.0f;
	float m_min_height = 0.0f;

	std::vector<ACTION*> m_actions;	
	int m_id = 0;
	
	int m_parent_id = -1;

	void save(tinyxml2::XMLDocument& doc,tinyxml2::XMLElement& element_doc);
	void load(tinyxml2::XMLDocument& doc,tinyxml2::XMLElement& element_doc);

};



#endif
