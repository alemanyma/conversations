#pragma once







/*
#ifndef __EAF_PARSER__H
#define __EAF_PARSER__H 1

//To look for
//<MEDIA_DESCRIPTOR MEDIA_URL="file:///C:/Users/Amparo/Desktop/Universidad/4º Carrera/Primer cuatrimestre/Español coloquial/Transcripción/Conversación grabada.WAV" MIME_TYPE="audio/x-wav" RELATIVE_MEDIA_URL="./Conversación grabada.WAV"/>

//<TIME_ORDER>
//<TIME_SLOT TIME_SLOT_ID = "ts1" TIME_VALUE = "28406" / >
//<TIME_SLOT TIME_SLOT_ID = "ts2" TIME_VALUE = "28806" / >
//<TIME_SLOT TIME_SLOT_ID = "ts3" TIME_VALUE = "28996" / >
//....
//< / TIME_ORDER>


#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <string>

#include <set>
#include <vector>

#include <algorithm>
#include <functional>

static int g_id = 0;

struct TimersData {
	std::map<std::string, float> TIME_SLOT_ID;
};



struct simpleTextInfo {
	float startTime;
	float endTime;
	std::string text;
	std::string AUTHOR;
	std::string id;
	unsigned int intervencion = 0;
	float y_ = 0.0f;
};



struct realText {
	std::set<std::pair<std::string, simpleTextInfo>, std::function<bool(std::pair<std::string, simpleTextInfo>, std::pair<std::string, simpleTextInfo>)>> speakerInfo;		
};

struct TextData {	
	std::map<std::string, simpleTextInfo> speakerInfo;
};

class Figure;
struct State {
	TimersData timers;
	realText textData;
	std::vector<Figure*> AllFigures;
};

void lookForTimes(const char *route, TimersData* location);
void lookForText(const char *route, realText& dst, TimersData* source);




#endif // !__EAF_PARSER__H
*/