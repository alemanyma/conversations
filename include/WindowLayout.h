#pragma once


#include "Figures.h"
#include <imgui/imgui.h>
#include "soloud_wav.h"
#include "soloud.h"
#include <string>

enum WINDOW_LAYOUT_TYPE 
{
	WLT_WINDOW_MAIN_EDIT_TEXT,					// all text is preset to the user and all lines are rendered	
	WLT_WINDOW_ADD_FIGURES,						// user select the figure from data side to be created 
	WLT_WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG,	// pop up window to generate new figure data
	WLT_WINDOW_POPUP_ADD_FIGURE,				// pop up to choose child figure or no child figure
	WLT_WINDOW_EDIT_FIGURES,					// window to edit current instanced figures data
	WLT_WINDOW_POPUP_EDIT_ENTRIES,
	WLT_WINDOW_MAIN_MENU,
	WLT_WINDOW_AUDIO,
	WLT_WINDOW_FILE_MANAGER,
	WLT_WINDOW_COUNT,
};


class WINDOW_LAYOUT
{
	friend class Instance;
public:
	WINDOW_LAYOUT() {};

	virtual void update() = 0;	// actual draw of each window

	void set_window_name(const std::string& name) { m_window_name = name; }
	void set_window_pos(const unsigned int x, const unsigned int y) { m_pos_x = x; m_pos_y = y; }
	void set_window_size(const unsigned int x, const unsigned int y) { m_size_x = x; m_size_y = y; }
	
	WINDOW_LAYOUT_TYPE m_window_type;
	int m_draw_priority = 0; // near to 0, more priority to be drawn, like z-index
	bool m_visible = true;

protected:
	inline void reset_cursor();
	inline void set_cursor(const sf::Vector2f& cursor_pos);

	void begin_window(); // must be called in pair, per each begin_window() , call end_window()
	void end_window();

	int m_window_mask = 0;

	unsigned int m_pos_x = 0;
	unsigned int m_pos_y = 0;
	unsigned int m_size_x = 256;
	unsigned int m_size_y = 256;

	std::string m_window_name;
	
	
};

struct MAIN_EDIT_WINDOW_DATA 
{
	bool m_adding_new_figure = false;
	FIGURE_ENUM m_current_figure_enum;
	bool m_scrolling_to_this_figure = false;
	ACTION_FIGURE_DATA* m_current_adding_action_data;
	std::shared_ptr<FIGURE_ACTIONS> m_current_edited_figure = nullptr;
	std::vector<ENTRY> m_all_entries;

	unsigned int m_vertical_ofset = 25;
	unsigned int m_horizontal_ofset = 200;

	float m_max_heigh = 0.0f;
	FIGURE_ACTIONS* m_scrolling_to_this_figure_instance = nullptr;
};

class WINDOW_MAIN_EDIT_TEXT : public WINDOW_LAYOUT
{
public:
	WINDOW_MAIN_EDIT_TEXT()
	{
		set_window_name("WINDOW_MAIN_EDIT_TEXT");		
		m_draw_priority = 0;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_EDIT_TEXT;		
	};

	virtual void update() override;	
	void add_figure_to_world(ACTION_FIGURE_DATA* data);
	void scroll_to_this_figure(FIGURE_ACTIONS* data);	
	MAIN_EDIT_WINDOW_DATA& get_data() { return m_data; }
	void set_entries(std::vector<ENTRY>& all_entries) { m_data.m_all_entries = all_entries; }
	void add_entry(ENTRY& entry) { m_data.m_all_entries.push_back(entry); }
	ENTRY& get_entry_by_id(int id);
private:
	void adding_new_figure_information();
	void handle_creation_of_figure(const ENTRY& new_entry, const ACTION_FIGURE_DATA& action, const int current_index);
	void handle_creation_of_figure_sibling(const ENTRY& new_entry, const ACTION_FIGURE_DATA& action, const int current_index, const int current_sibling_index);
	void draw_all_figures();
	void draw_recursive_figures(std::shared_ptr<FIGURE_ACTIONS> figure_actions, const int level);
	MAIN_EDIT_WINDOW_DATA m_data;
	
	
};

class WINDOW_ADD_FIGURES : public WINDOW_LAYOUT
{
public:
	WINDOW_ADD_FIGURES()
	{
		set_window_name("WINDOW_ADD_FIGURES");
		m_draw_priority = 0;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_ADD_FIGURES;
	};

	virtual void update() override;

private:
	
};

class WINDOW_AUDIO : public WINDOW_LAYOUT
{
public:
	WINDOW_AUDIO()
	{
		set_window_name("WINDOW_AUDIO");
		m_draw_priority = 0;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_AUDIO;
	};

	virtual void update() override;

private:
	
};

enum FILE_MANAGER_SERIALIZE_OPTION
{
	FMSO_default_file_figues_data_string,
	FMSO_default_file_colour_profile_string,
	FMSO_default_file_entries_string,
	FMSO_default_file_web_solution_string,
	FMSO_default_file_figures_use_string,
	FMSO_serialized_string,
	FMSO_unselected
};

class WINDOW_MAIN_MENU : public WINDOW_LAYOUT
{
public:
	WINDOW_MAIN_MENU()
	{
		set_window_name("WINDOW_MAIN_MENU");
		m_draw_priority = 0;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_MAIN_MENU;
	};

	virtual void update() override;
	static void save(std::string& path,FILE_MANAGER_SERIALIZE_OPTION option_selected);
	static void load(const std::string& path,FILE_MANAGER_SERIALIZE_OPTION option_selected);

	static std::string m_default_option_selected[6];
private:
	// serialization
	static std::string m_relative_path;
	static std::string m_default_file_figues_data;
	static std::string m_default_file_colour_profile;
	static std::string m_default_file_entries;
	static std::string m_default_file_web_solution;
	static std::string m_default_file_figures_use;
	static std::string m_serialized_data;
	

	static std::string m_default_file_figues_data_string;
	static std::string m_default_file_colour_profile_string;
	static std::string m_default_file_entries_string;
	static std::string m_default_file_web_solution_string;
	static std::string m_default_file_figures_use_string;
	static std::string m_serialized_string;
};


class WINDOW_EDIT_FIGURES : public WINDOW_LAYOUT
{
public:
	WINDOW_EDIT_FIGURES()
	{
		set_window_name("WINDOW_EDIT_FIGURES");
		m_draw_priority = 0;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_EDIT_FIGURES;
	};

	virtual void update() override;
private:
	void recursive_draw(std::shared_ptr<FIGURE_ACTIONS> fig, const float space = 0.0f);
};

class WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG : public WINDOW_LAYOUT
{
public:
	WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG()
	{
		set_window_name("WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG");
		m_draw_priority = 1;
		m_visible = false;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG;
	};

	virtual void update() override;	
	
private:	
};

class WINDOW_POPUP_EDIT_ENTRIES : public WINDOW_LAYOUT
{
public:
	WINDOW_POPUP_EDIT_ENTRIES()
	{
		set_window_name("WINDOW_POPUP_CREATE_DATA_FIGURE_CONFIG");
		m_draw_priority = 1;
		m_visible = false;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_EDIT_ENTRIES;
	};

	virtual void update() override;	
	
	ENTRY* m_editable_entry = nullptr;
	
private:	
};

class WINDOW_POPUP_ADD_FIGURE : public WINDOW_LAYOUT
{
public:
	WINDOW_POPUP_ADD_FIGURE()
	{
		set_window_name("WINDOW_POPUP_ADD_FIGURE");
		m_draw_priority = 1;
		m_visible = false;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_POPUP_ADD_FIGURE;
	};

	virtual void update() override;

private:
	void recursive_draw_child_figures(FIGURE_ACTIONS& fig, const int spaces, FIGURE_ACTIONS** to_return);
	// simple figure
	// complex figure
};

class WINDOW_FILE_MANAGER : public WINDOW_LAYOUT
{
public:
	WINDOW_FILE_MANAGER()
	{
		set_window_name("WINDOW_FILE_MANAGER");
		m_draw_priority = 1;
		m_visible = false;
		m_window_type = WINDOW_LAYOUT_TYPE::WLT_WINDOW_FILE_MANAGER;
	};

	virtual void update() override;

private:
};

struct COLOUR_PROFILES 
{
	std::vector<sf::Color> m_colours;
	COLOUR_PROFILES();
	
};

struct FIGURES_INSTANCE 
{
	std::vector<std::shared_ptr<FIGURE_ACTIONS>>	 m_all_figures;	
	std::shared_ptr<FIGURE_ACTIONS>					 figure_to_create;

	void hierarchy_check();
};

class Instance
{
public:
	static Instance& inst()
	{
		static Instance* singleton = new Instance();
		return *singleton;
	}
	
	Instance::Instance() 
	{
		m_all_windows.resize(WINDOW_LAYOUT_TYPE::WLT_WINDOW_COUNT);
		m_figure_instances.resize(1);
	}
	WINDOW_LAYOUT* window_layout(WINDOW_LAYOUT_TYPE wt)
	{
		for (auto&& w : m_all_windows) 
		{
			if (w->m_window_type == wt)return w;
		}

		return nullptr;
	}

	FIGURES_INSTANCE& get_current_figures_instances() 
	{
		return m_figure_instances[m_current_instances_id];
	}

	void set_audio(const char* audio_path);
	void play_audio();

	void add_new_window(WINDOW_LAYOUT* new_window)
	{ 
		m_all_windows[new_window->m_window_type] = new_window;
	};
	
	void mask_all(const int mask_to_apply);
	void or_all(const int mask_to_apply);
	void lock_all_but_one(WINDOW_LAYOUT* keep_unlocked);
	void resort_all_windows();

	void open_pop_up_window(const WINDOW_LAYOUT_TYPE window_enum);
	void return_input_all_windows();
	
	void set_adding_new_figure_to_main_window(const bool b, const FIGURE_ENUM& current_figure_enum);
	void scroll_main_window_looking_for_figure(FIGURE_ACTIONS* figure);
	void create_new_data_figure(ACTION_FIGURE_DATA& data);
	void create_new_figure_instance(FIGURE_ACTIONS* parent);
	
	void set_edit_entry(ENTRY* entry_to_edit);
	
	COLOUR_PROFILES m_colour_profile;

	Vector2f m_window_size;
	std::vector<WINDOW_LAYOUT*> m_all_windows;

	int m_current_instances_id = 0;
	std::vector<FIGURES_INSTANCE> m_figure_instances;

	std::vector<std::shared_ptr<ACTION_FIGURE_DATA>> m_all_actions_data;
	std::shared_ptr<ACTION_FIGURE_DATA> get_data_by_name(const std::string& name);
	//std::vector<std::shared_ptr<FIGURE_ACTIONS>> m_all_figures;
	//std::vector<std::shared_ptr<ACTION_FIGURE_DATA>> m_all_actions_data;
	//std::shared_ptr<FIGURE_ACTIONS> figure_to_create;

	float m_max_heigh = 0.0f;
	float m_max_time = 0.0f;
	SoLoud::Wav m_audio;
	std::string m_audio_path = "../../audios/test.wav";
	SoLoud::Soloud soloud;  
	float m_audio_lenght = 0.0f;  
	float m_audio_height_main_window = 0.0f;  
	float m_audio_currentl_time = 0.0f;  
	int audio_handle = -1;  
	bool m_save = false;  
};

//open and read document, parse and return one object containing all the relevant data
std::vector<ENTRY> parse_file(std::string path);

